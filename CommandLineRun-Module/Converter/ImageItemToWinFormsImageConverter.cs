﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module.Converter
{
    public static class ImageItemToWinFormsImageConverter
    {
        public static Image Convert(object imageItem)
        {
            if (imageItem is Image)
            {
                return (Image)imageItem;
            }
            else
            {
                return null;
            }
        }
    }
}

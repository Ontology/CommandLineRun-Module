﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandLineRun_Module.Converter
{
    public static class FormCloseTypeToDialogResultConverter
    {
        public static DialogResult Convert(CloseType closeType)
        {
            switch(closeType)
            {
                case CloseType.OK:
                    return DialogResult.OK;
                case CloseType.Cancel:
                    return DialogResult.Cancel;
                default:
                    return DialogResult.None;
            }

            
        }
    }
}

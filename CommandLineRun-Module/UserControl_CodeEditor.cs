﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using LocalizedTemplate_Module;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using CommandLineRun_Module.Converter;

namespace CommandLineRun_Module
{
    public partial class UserControl_CodeEditor : UserControl
    {
        private delegate void viewModelChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e);

        private CodeEditor_ViewModel viewModel;

        private frmAutoCorrection objFrmAutoCorrection;


        public event SavedCodeSnipplet savedCodeSnipplet;


        public void Clear()
        {
            viewModel.Clear();

        }

        public UserControl_CodeEditor(clsLocalConfig LocalConfig)
        {
            InitializeComponent();

            viewModel = new CodeEditor_ViewModel(LocalConfig);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            viewModel.savedCodeSnipplet += ViewModel_savedCodeSnipplet;
            viewModel.InitializeViewModel();

        }

        private void ViewModel_savedCodeSnipplet(clsOntologyItem OItem_CodeSnipplet)
        {
            if (savedCodeSnipplet != null)
            {
                savedCodeSnipplet(OItem_CodeSnipplet);
            }
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var delegPropertyChange = new viewModelChanged(ViewModel_PropertyChanged);
                this.Invoke(delegPropertyChange, sender, e);
            }
            else
            {
                if (e.PropertyName == NotifyChanges.CodeEditorViewModel_IsEnabled_Save)
                {
                    toolStripButton_Save.Enabled = viewModel.IsEnabled_Save;
                    
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_IsEnabled_ReplaceVar)
                {
                    toolStripButton_ReplaceVariables.Enabled = viewModel.IsEnabled_ReplaceVar;
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_IsEnabledCodeEditor)
                {
                    scintilla_Code.Enabled = viewModel.IsEnabledCodeEditor;
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_CodeText)
                {
                    scintilla_Code.Text = viewModel.CodeText;
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_FormMessage)
                {
                    MessageBox.Show(this, viewModel.FormMessage.Message, viewModel.FormMessage.Short, FormMessageToButtonConverter.Convert(viewModel.FormMessage), FormMessageToIconConverter.Convert(viewModel.FormMessage));
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_CompareItem)
                {
                    if (scintilla_Code.GetRange(viewModel.CompareItem.IxStart, viewModel.CompareItem.End).Text == viewModel.CompareItem.TextToCompare)
                    {
                        scintilla_Code.Selection.Start = viewModel.CompareItem.IxStart;
                        scintilla_Code.Selection.End = viewModel.CompareItem.End;
                        scintilla_Code.Selection.Text = viewModel.CompareItem.TextToExchange;

                    }
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_ImageItemLock)
                {
                    toolStripButton_Lock.Image = ImageItemToWinFormsImageConverter.Convert(viewModel.ImageItemLock);
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_SyntxHighlighter)
                {
                    scintilla_Code.ConfigurationManager.Language = viewModel.SyntxHighlighter;
                }
                else if (e.PropertyName == NotifyChanges.CodeEditorViewModel_LineCount)
                {
                    toolStripLabel_LineCount.Text = viewModel.LineCountLabel;
                }
            }
        }

        public UserControl_CodeEditor(Globals globals)
        {
            InitializeComponent();
            viewModel = new CodeEditor_ViewModel(globals);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            viewModel.InitializeViewModel();
            
        }

        public void Initialize_CodeSnipplet(clsOntologyItem OItem_CodeSnipplet, clsOntologyItem OItem_ProgrammingLanguage = null)
        {
            ParentForm.FormClosing += UserControl_CodeEditor_FormClosing;
            viewModel.OItem_CodeSnipplet = OItem_CodeSnipplet;
            viewModel.OItem_ProgrammingLanguage = OItem_ProgrammingLanguage;

            viewModel.GetCodeSnipplet();
            viewModel.GetSyntaxHighlight();

            viewModel.IsEditorLocked = true;
        }

        void UserControl_CodeEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (toolStripButton_Save.Enabled)
            {
                viewModel.SaveCode(scintilla_Code.Text);
            }
        }

       

        private void toolStripButton_ReplaceVariables_Click(object sender, EventArgs e)
        {
            viewModel.ReplaceVariables();
        }

        

        private void scintilla_Code_Leave(object sender, EventArgs e)
        {
            viewModel.SaveCode(scintilla_Code.Text);
        }

        private void toolStripButton_Lock_Click(object sender, EventArgs e)
        {
            viewModel.IsEditorLocked = !viewModel.IsEditorLocked;

        }

        

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        { 
            viewModel.SaveCode(scintilla_Code.Text);
            
        }


        private void scintilla_Code_TextChanged(object sender, EventArgs e)
        {
            viewModel.TextChanged(scintilla_Code.Lines.Count);
            
        }

        private void scintilla_Code_KeyDown(object sender, KeyEventArgs e)
        {
            if (Control.ModifierKeys.HasFlag(Keys.Control))
            {
                if (e.KeyCode == Keys.Space)
                {
                    if (viewModel.OItem_ProgrammingLanguage != null)
                    {
                        if (objFrmAutoCorrection == null)
                        {
                            objFrmAutoCorrection = new frmAutoCorrection(viewModel.LocalConfig.Globals);
                            objFrmAutoCorrection.selectedCorrectorItem += objFrmAutoCorrection_selectedCorrectorItem;
                            viewModel.AutoCorrectorUsable = objFrmAutoCorrection.SetAutoCorrectorByRef(viewModel.OItem_ProgrammingLanguage);
                        }

                        viewModel.PositionOfWord = scintilla_Code.Caret.Position;
                        viewModel.WordOfPosition = scintilla_Code.GetWordFromPosition(viewModel.PositionOfWord);
                        objFrmAutoCorrection.ValueToSearch = viewModel.WordOfPosition;
                        objFrmAutoCorrection.Show();


                    }
                    
                }
            }
        }

        void objFrmAutoCorrection_selectedCorrectorItem(frmAutoCorrection frmAutoCorrection, clsOntologyItem oItem_Selected)
        {
            frmAutoCorrection.Hide();

            if (oItem_Selected != null)
            {
                var wordToCompare = scintilla_Code.GetWordFromPosition(viewModel.PositionOfWord);
                viewModel.CorrectorItem(wordToCompare, oItem_Selected);
                
            }
        }


    }
}

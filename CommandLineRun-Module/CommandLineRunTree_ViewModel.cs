﻿using ClassLibrary_ShellWork;
using CommandLineRun_Module.BaseClasses;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public enum MarkTimerActionType
    {
        Stop = 0,
        Start = 1
    }

    public delegate void ClearCommandLineTreeNode();
    public class CommandLineRunTree_ViewModel : ViewModelBase
    {
        public clsLocalConfig LocalConfig
        {
            get; private set;
        }

        private clsOntologyItem objOItem_Ref;

        

        private clsDataWork_CommandLineRun objDataWork_CommandLineRun;
        public clsDataWork_CommandLineRun DataWork_CommandLineRun
        {
            get { return objDataWork_CommandLineRun; }
            set
            {
                objDataWork_CommandLineRun = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_DataWork_CommandLineRun);
            }
        }
        private clsShellWork objShellWork;
        public clsShellWork ShellWork
        {
            get { return objShellWork; }
            private set
            {
                objShellWork = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_ShellWork);
            }
        }

        private bool isApplyable;
        public bool IsApplyable
        {
            get { return isApplyable; }
            set
            {
                isApplyable = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_IsApplyable);
            }
        }

        private CommandLineRunTreeItem selectedTreeItem;
        public CommandLineRunTreeItem SelectedTreeItem
        {
            get { return selectedTreeItem; }
            set
            {
                selectedTreeItem = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_SelectedTreeItem);
            }
        }

        private CommandLineRunTreeItem treeItemRoot;
        public CommandLineRunTreeItem TreeItemRoot
        {
            get { return treeItemRoot; }
            set
            {
                treeItemRoot = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_TreeItemRoot);
            }
        }

        private Color nodeBackColor;
        public Color NodeBackColor
        {
            get { return nodeBackColor; }
            set
            {
                nodeBackColor = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_NodeBackColor);
            }
        }
        private Color nodeForeColor;
        public Color NodeForeColor
        {
            get { return nodeForeColor; }
            set
            {
                nodeForeColor = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_NodeForeColor);
            }
        }


        private Color nodeBackColor_marked = System.Drawing.Color.Yellow;
        public Color NodeBackColor_marked
        {
            get { return nodeBackColor_marked; }
            set
            {
                nodeBackColor_marked = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_NodeBackColor_marked);
            }
        }

        private string nodeIdToFind;
        public string NodeIdToFind
        {
            get { return nodeIdToFind; }
            set
            {
                nodeIdToFind = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_NodeIdToFind);
            }
        }

        private string treeNodeCount;
        public string TreeNodeCount
        {
            get { return treeNodeCount; }
            set
            {
                treeNodeCount = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_TreeNodeCount);
            }
        }

        private MarkTimerActionType markTimerActionType;
        public MarkTimerActionType MarkTimerActionType
        {
            get { return markTimerActionType; }
            set
            {
                markTimerActionType = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_MarkTimerActionType);
            }
        }

        private List<CommandLineRunTreeItem> treeItems = new List<CommandLineRunTreeItem>();
        public List<CommandLineRunTreeItem> TreeItems
        {
            get { return treeItems; }
            set
            {
                treeItems = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_TreeItems);
            }
        }

        private List<CommandLineRunTreeItem> treeItemsToUnmark = new List<CommandLineRunTreeItem>();
        public List<CommandLineRunTreeItem> TreeItemsToUnmark
        {
            get { return treeItemsToUnmark; }
            set
            {
                treeItemsToUnmark = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_TreeItemsToUnmark);
            }
        }

        private List<CommandLineRunTreeItem> treeItemsToMark = new List<CommandLineRunTreeItem>();
        public List<CommandLineRunTreeItem> TreeItemsToMark
        {
            get { return treeItemsToMark; }
            set
            {
                treeItemsToMark = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_TreeItemsToMark);
            }
        }

        private List<CommandLineRunTreeItem> treeItemsToExpandParents = new List<CommandLineRunTreeItem>();
        public List<CommandLineRunTreeItem> TreeItemsToExpandParents
        {
            get { return treeItemsToExpandParents; }
            set
            {
                treeItemsToExpandParents = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_TreeItemsToExpandParents);
            }
        }

        private List<clsOntologyItem> objectsToEdit = new List<clsOntologyItem>();
        public List<clsOntologyItem> ObjectsToEdit
        {
            get { return objectsToEdit; }
            set
            {
                objectsToEdit = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_ObjectsToEdit);
            }
        }

        private clsOntologyItem oItemToSendToModule;
        public clsOntologyItem OItemToSendToModule
        {
            get { return oItemToSendToModule; }
            set
            {
                oItemToSendToModule = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_OItemToSendToModule);
            }
        }

        private clsOntologyItem result_SemFilter;
        public clsOntologyItem Result_SemFilter
        {
            get { return result_SemFilter; }
            set
            {
                result_SemFilter = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_Result_SemFilter);
            }
        }

        private string semFilter;
        public string SemFilter
        {
            get { return semFilter; }
            set
            {
                semFilter = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_SemFilter);
            }
        }

        private string lastTreeNodeFullPath = "";
        public string LastTreeNodeFullPath
        {
            get { return lastTreeNodeFullPath; }
            set
            {
                lastTreeNodeFullPath = value;
            }
        }


        private bool isEnabled_ModuleMenuItem;
        public bool IsEnabled_ModuleMenuItem
        {
            get { return isEnabled_ModuleMenuItem; }
            set
            {
                isEnabled_ModuleMenuItem = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_IsEnabled_ModuleMenuItem);
            }
        }

        private bool isEnabled_Refresh;
        public bool IsEnabled_Refresh
        {
            get { return isEnabled_Refresh; }
            set
            {
                isEnabled_Refresh = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_IsEnabled_Refresh);
            }
        }

        private bool isEnabled_MapVariables;
        public bool IsEnabled_MapVariables
        {
            get { return isEnabled_MapVariables; }
            set
            {
                isEnabled_MapVariables = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_IsEnabled_MapVariables);
            }
        }

        private clsOntologyItem oItemApplied;
        public clsOntologyItem OItemApplied
        {
            get { return oItemApplied; }
            set
            {
                oItemApplied = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_OItemApplied);
            }
        }

        private FormMessage formMessage;
        public FormMessage FormMessage
        {
            get
            {
                return formMessage;
            }
            set
            {
                formMessage = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_FormMessage);
            }
        }

        private bool refreshItem;
        public bool RefreshItem
        {
            get
            {
                return refreshItem;
            }
            set
            {
                refreshItem = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_RefreshItem);
            }
        }

        private clsOntologyItem oItemToMapVarToVal;
        public clsOntologyItem OItemToMapVarToVal
        {
            get { return oItemToMapVarToVal; }
            set
            {
                oItemToMapVarToVal = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_OItemToMapVarToVal);
            }
        }

        public void UnmarkNodes()
        {
            TreeItemsToUnmark = TreeItems;
        }

        public void SearchNode(string search)
        {
            if (LocalConfig.Globals.is_GUID(search))
            {
                search = search.Replace("-", "").ToLower();
                TreeItemsToMark = TreeItems.Where(treeItem => treeItem.IdCommandLineRun.ToLower() == search).ToList();
                
            }
            else
            {
                search = search.ToLower();
                TreeItemsToMark = TreeItems.Where(treeItem => treeItem.NameCommandLineRun.ToLower().Contains(search)).ToList();
            }
        }


        private void SearchNodeAsync()
        {

        }

        private bool searchIsRunning;

        private string strLastModule;

        public void SelectNode(string idNode)
        {
            var nodeItem = TreeItems.FirstOrDefault(treeItem => treeItem.IdCommandLineRun == idNode);
            if (nodeItem != null)
            {
                SelectedTreeItem = nodeItem;
            }
        }

        public void TreeItemDoubleClick(string idTreeItem)
        {
            if (idTreeItem != LocalConfig.Globals.Root.GUID)
            {
                var oItem = TreeItems.Where(treeItem => treeItem.IdCommandLineRun == idTreeItem).Select(treeItem => new clsOntologyItem
                {
                    GUID = treeItem.IdCommandLineRun,
                    Name = treeItem.NameCommandLineRun,
                    GUID_Parent = LocalConfig.OItem_class_comand_line__run_.GUID,
                    Type = LocalConfig.Globals.Type_Object
                }).FirstOrDefault();

                if (oItem != null)
                {
                    ObjectsToEdit = new List<clsOntologyItem> { oItem };
                }
            }
        }

        public void SendItemToModule(string idTreeItem)
        {
            if (idTreeItem != LocalConfig.Globals.Root.GUID)
            {
                var oItem = TreeItems.Where(treeItem => treeItem.IdCommandLineRun == idTreeItem).Select(treeItem => new clsOntologyItem
                {
                    GUID = treeItem.IdCommandLineRun,
                    Name = treeItem.NameCommandLineRun,
                    GUID_Parent = LocalConfig.OItem_class_comand_line__run_.GUID,
                    Type = LocalConfig.Globals.Type_Object
                }).FirstOrDefault();

                if (oItem != null)
                {
                    OItemToSendToModule = oItem;
                }
            }
        }

        public void RefreshTreeItem(string idTreeItem)
        {
            if (!string.IsNullOrEmpty(idTreeItem) && idTreeItem != TreeItemRoot.IdCommandLineRun)
            {
                RefreshItem = true;
            }
        }

        public void SetMapVariableToValueItem(string idTreeItem)
        {
            if (!string.IsNullOrEmpty(idTreeItem) && idTreeItem != TreeItemRoot.IdCommandLineRun)
            {
                var cmdlrItem = TreeItems.Where(treeItem => treeItem.IdCommandLineRun == idTreeItem).Select(treeItem => new clsOntologyItem
                {
                    GUID = treeItem.IdCommandLineRun,
                    Name = treeItem.NameCommandLineRun,
                    GUID_Parent = LocalConfig.OItem_class_comand_line__run_.GUID,
                    Type = LocalConfig.Globals.Type_Object
                }).FirstOrDefault();

                if (cmdlrItem != null)
                {
                    OItemToMapVarToVal = cmdlrItem;
                }
            }
        }

        public void RefreshCodes()
        {
            objDataWork_CommandLineRun.GetSubData_013_Codes();
            RefreshItem = true;
        }

        public void AddSemFilter(clsOntologyItem oItemClass, clsOntologyItem oItemDirection, clsOntologyItem oItemRelationType, clsOntologyItem oItemObject)
        {
            objDataWork_CommandLineRun.OItem_Class = oItemClass;
            objDataWork_CommandLineRun.OItem_Direction = oItemDirection;
            objDataWork_CommandLineRun.OItem_RelationType = oItemRelationType;
            objDataWork_CommandLineRun.OItem_Object = oItemObject;


            Result_SemFilter = objDataWork_CommandLineRun.GetData_CommandLineRun();

            var semFilter = "";
            if (Result_SemFilter.GUID == LocalConfig.Globals.LState_Success.GUID)
            {
                if (objDataWork_CommandLineRun.OItem_Direction != null)
                {
                    semFilter = objDataWork_CommandLineRun.OItem_Direction.GUID ==
                                   LocalConfig.Globals.Direction_LeftRight.GUID
                                       ? "o >"
                                       : "o <";

                    semFilter += " | cl: " + (objDataWork_CommandLineRun.OItem_Class != null
                                        ? objDataWork_CommandLineRun.OItem_Class.Name
                                        : "");

                    semFilter += " | rel: " + (objDataWork_CommandLineRun.OItem_RelationType != null
                                        ? objDataWork_CommandLineRun.OItem_RelationType.Name
                                        : "");

                    semFilter += " | obj: " + (objDataWork_CommandLineRun.OItem_Object != null
                                        ? objDataWork_CommandLineRun.OItem_Object.Name
                                        : "");


                }

            }

            SemFilter = semFilter;
        }



        public void ReconfigureEnableStates(string idSelectedNode = null)
        {
            IsApplyable = false;
            IsEnabled_ModuleMenuItem = false;
            IsEnabled_Refresh = false;
            IsEnabled_MapVariables = false;

            if (!string.IsNullOrEmpty(idSelectedNode) && idSelectedNode != TreeItemRoot.IdCommandLineRun)
            {
                IsApplyable = true;
                IsEnabled_ModuleMenuItem = true;
                IsEnabled_Refresh = true;
                IsEnabled_MapVariables = true;
            }
        }

        public void AppliedNode(string idNode)
        {
            if (!string.IsNullOrEmpty(idNode) && idNode != TreeItemRoot.IdCommandLineRun)
            {
                var oItem = TreeItems.Where(treeItem => treeItem.IdCommandLineRun == idNode).Select(treeItem => new clsOntologyItem
                {
                    GUID = treeItem.IdCommandLineRun,
                    Name = treeItem.NameCommandLineRun,
                    GUID_Parent = LocalConfig.OItem_class_comand_line__run_.GUID,
                    Type = LocalConfig.Globals.Type_Object
                }).FirstOrDefault();

                if (oItem != null)
                {
                    if (LocalConfig.objOItem_Session != null)
                    {
                        LocalConfig.objSession.RegisterItems(LocalConfig.objOItem_Session, new List<clsOntologyItem> { oItem }, false);
                    }

                    OItemApplied = oItem;
                }

                
            }
        }

        public void MarkNodesByList(List<clsOntologyItem> commandLineRunList)
        {
            var toDoCount = commandLineRunList.Count;
            var done = 0;

            var markList = (from toMarkItem in commandLineRunList
                            join existingItem in TreeItems on toMarkItem.GUID equals existingItem.IdCommandLineRun
                            select existingItem).ToList();
            done = markList.Count;

            if (done < toDoCount)
            {
                FormMessage = new FormMessage("Es konnten nur " + done + " von " + toDoCount + " Items ermittelt werden!", "Fehler!", FormMessageType.OK | FormMessageType.Information);
            }

            if (markList.Any())
            {
                TreeItemsToUnmark = TreeItems;
                TreeItemsToMark = markList;
                TreeItemsToExpandParents = markList;
            }
        }

        public CommandLineRunTree_ViewModel(clsLocalConfig localConfig, clsDataWork_CommandLineRun dataWork_CommandLineRun)
        {
            LocalConfig = localConfig;
            DataWork_CommandLineRun = dataWork_CommandLineRun;
            
        }

        public void IntializeForm()
        {
            objShellWork = new clsShellWork();
            IsApplyable = false;
            
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public class clsFieldReplace
    {
        public string ID_FieldReplace { get; set; }
        public string Name_FieldReplace { get; set; }
        public string ID_Attribute_Text { get; set; }
        public string Text { get; set; }
        public string ID_RegularExpression { get; set; }
        public string Name_RegularExpression { get; set; }
        public string ID_Attribute_Regex { get; set; }
        public string Regex { get; set; }

        public string Replace(string value)
        {
            var regexItem = new Regex(Regex);
            Text = Text ?? "";
            return regexItem.Replace(value, Text);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public class clsVariableToField
    {
        public string ID_VariableAndField { get; set; }
        public string Name_VariableAndField { get; set; }
        public string ID_Variable { get; set; }
        public string Name_Variable { get; set; }
        public string ID_Field { get; set; }
        public string Name_Field { get; set; }
        public string ID_CommandLineRun { get; set; }
        public string Name_CommandLineRun { get; set; }

        private List<clsRemoveCharacter> removeCharacters = new List<clsRemoveCharacter>();
        public List<clsRemoveCharacter> RemoveCharacters
        {
            get { return removeCharacters; }
            set
            {
                removeCharacters = value;
            }
        }

        private List<clsFieldReplace> fieldReplaces = new List<clsFieldReplace>();
        public List<clsFieldReplace> FieldReplaces
        {
            get { return fieldReplaces; }
            set
            {
                fieldReplaces = value;
            }
        }
    }
}

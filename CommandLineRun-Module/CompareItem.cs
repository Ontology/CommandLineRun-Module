﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public class CompareItem
    {
        public int IxStart { get; set; }
        public int End { get; set; }
        public string TextToCompare { get; set; }
        public string TextToExchange { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Ontology_Module;

namespace CommandLineRun_Module
{
    public class clsDataWork_TextPOrReportsToCommandLine
    {
        private clsLocalConfig objLocalConfig;

        private clsOntologyItem objOItem_TextParserOrReport;


        private OntologyModDBConnector objDBLevel_CommandLineToTextPOrReport;
        private OntologyModDBConnector objDBLevel_CommandLineRun;
        private OntologyModDBConnector objDBLevel_VarToField;
        private OntologyModDBConnector objDBLevel_VarToField_FieldAndValue;
        private OntologyModDBConnector objDBLevel_RemoveCharacters;
        private OntologyModDBConnector objDBLevel_RemoveCharacters_Attributes;
        private OntologyModDBConnector objDBLevel_FieldReplace;
        private OntologyModDBConnector objDBLevel_FieldReplaceText;
        private OntologyModDBConnector objDBLevel_RegularExpressions;
        private OntologyModDBConnector objDBLevel_RegEx;

        private clsOntologyItem OItem_Result_CommandLineToReportOrTextP;
        private clsOntologyItem OItem_Result_CommandLineRun;
        private clsOntologyItem OItem_Result_VarToField;
        private clsOntologyItem OItem_Result_FieldAndValue;
        private clsOntologyItem OItem_Result_RemoveCharacters;
        private clsOntologyItem OItem_Result_RemoveCharacters_Attributes;
        private clsOntologyItem OItem_Result_FieldReplace;
        private clsOntologyItem OItem_Result_FieldReplaceText;
        private clsOntologyItem OItem_Result_RegularExpressions;
        private clsOntologyItem OItem_Result_RegEx;


        private bool forReports;

        public List<clsRemoveCharacter> RemoveCharactersCmdLineRun { get; set; }
        
        public List<clsOntologyItem> CommandLineRunList
        {
            get
            {
                var cmdltr = (from cmdlr in objDBLevel_CommandLineRun.ObjectRels
                              group cmdlr by new clsOntologyItem
                                  {
                                      GUID = cmdlr.ID_Other,
                                      Name = cmdlr.Name_Other,
                                      GUID_Parent = cmdlr.ID_Parent_Other,
                                      Type = objLocalConfig.Globals.Type_Object
                                  } into cmdlrs
                              select cmdlrs.Key).ToList();

                return cmdltr;
            }
        }

        public List<clsVariableToField> GetVariableToField()
        {
            var variableToField = (from objVariableToField in objDBLevel_VarToField.ObjectRels
                                   join objVariable in
                                       objDBLevel_VarToField_FieldAndValue.ObjectRels.Where(
                                           vartofield =>
                                           vartofield.ID_Parent_Other == objLocalConfig.OItem_class_variable.GUID)
                                                                                .ToList() on
                                       objVariableToField.ID_Object equals objVariable.ID_Object
                                   join objReportFiled in
                                       objDBLevel_VarToField_FieldAndValue.ObjectRels.Where(
                                           vartofield =>
                                           vartofield.ID_Parent_Other == (forReports ? objLocalConfig.OItem_class_report_field.GUID : objLocalConfig.OItem_class_field.GUID))
                                                                                .ToList() on
                                       objVariableToField.ID_Object equals objReportFiled.ID_Object
                                   join cmdlrToRep in objDBLevel_CommandLineToTextPOrReport.ObjectRels on objVariableToField.ID_Other equals  cmdlrToRep.ID_Object
                                   join cmdlr in objDBLevel_CommandLineRun.ObjectRels on cmdlrToRep.ID_Object equals  cmdlr.ID_Object
                                   select new clsVariableToField
                                       {
                                           ID_VariableAndField = objVariableToField.ID_Object,
                                           Name_VariableAndField = objVariableToField.Name_Object,
                                           ID_Variable = objVariable.ID_Other,
                                           Name_Variable = objVariable.Name_Other,
                                           ID_Field = objReportFiled.ID_Other,
                                           Name_Field = objReportFiled.Name_Other,
                                           ID_CommandLineRun =  cmdlr.ID_Other,
                                           Name_CommandLineRun = cmdlr.Name_Other
                                       }).ToList();

            variableToField.ForEach(variableToFieldItem =>
            {
                variableToFieldItem.RemoveCharacters = (from removeCharacter in objDBLevel_RemoveCharacters.ObjectRels
                                        where removeCharacter.ID_Object == variableToFieldItem.ID_VariableAndField
                                        join beginAt in objDBLevel_RemoveCharacters_Attributes.ObjAtts.Where(ba => ba.ID_AttributeType == objLocalConfig.OItem_attributetype_at_begin.GUID) on removeCharacter.ID_Other equals beginAt.ID_Object
                                        join countItem in objDBLevel_RemoveCharacters_Attributes.ObjAtts.Where(ba => ba.ID_AttributeType == objLocalConfig.OItem_attributetype_count.GUID) on removeCharacter.ID_Other equals countItem.ID_Object
                                        join offsetItem in objDBLevel_RemoveCharacters_Attributes.ObjAtts.Where(ba => ba.ID_AttributeType == objLocalConfig.OItem_attributetype_offset.GUID) on removeCharacter.ID_Other equals offsetItem.ID_Object
                                        select new clsRemoveCharacter
                                        {
                                            ID_RemoveCharacter = removeCharacter.ID_Other,
                                            Name_RemoveCharacter = removeCharacter.Name_Other,
                                            AtBegin = beginAt.Val_Bit.Value,
                                            Count = countItem.Val_Lng.Value,
                                            Offset = countItem.Val_Lng.Value
                                        }).ToList();

                variableToFieldItem.FieldReplaces = (from fieldReplace in objDBLevel_FieldReplace.ObjectRels
                                                     where fieldReplace.ID_Object == variableToFieldItem.ID_VariableAndField
                                                     join fieldReplaceText in objDBLevel_FieldReplaceText.ObjAtts on fieldReplace.ID_Other equals fieldReplaceText.ID_Object
                                                     join regularExpression in objDBLevel_RegularExpressions.ObjectRels on fieldReplace.ID_Other equals regularExpression.ID_Object
                                                     join regEx in objDBLevel_RegEx.ObjAtts on regularExpression.ID_Other equals regEx.ID_Object
                                                     select new clsFieldReplace
                                                     {
                                                         ID_FieldReplace = fieldReplace.ID_Other,
                                                         Name_FieldReplace = fieldReplace.Name_Other,
                                                         ID_Attribute_Text = fieldReplaceText.ID_AttributeType,
                                                         Text = fieldReplaceText.Val_String,
                                                         ID_RegularExpression = regularExpression.ID_Other,
                                                         Name_RegularExpression = regularExpression.Name_Other,
                                                         ID_Attribute_Regex = regEx.ID_AttributeType,
                                                         Regex = regEx.Val_String
                                                     }).ToList();

            });

            RemoveCharactersCmdLineRun = (from reportRef in objDBLevel_CommandLineRun.ObjectRels
                                                              join removeCharacter in objDBLevel_RemoveCharacters.ObjectRels on reportRef.ID_Object equals removeCharacter.ID_Object
                                                              join beginAt in objDBLevel_RemoveCharacters_Attributes.ObjAtts.Where(ba => ba.ID_AttributeType == objLocalConfig.OItem_attributetype_at_begin.GUID) on removeCharacter.ID_Other equals beginAt.ID_Object
                                                              join countItem in objDBLevel_RemoveCharacters_Attributes.ObjAtts.Where(ba => ba.ID_AttributeType == objLocalConfig.OItem_attributetype_count.GUID) on removeCharacter.ID_Other equals countItem.ID_Object
                                                              join offsetItem in objDBLevel_RemoveCharacters_Attributes.ObjAtts.Where(ba => ba.ID_AttributeType == objLocalConfig.OItem_attributetype_offset.GUID) on removeCharacter.ID_Other equals offsetItem.ID_Object
                                                              group reportRef by new
                                                              {
                                                                  ID_RemoveCharacter = removeCharacter.ID_Other,
                                                                  Name_RemoveCharacter = removeCharacter.Name_Other,
                                                                  AtBegin = beginAt.Val_Bit.Value,
                                                                  Count = countItem.Val_Lng.Value,
                                                                  Offset = countItem.Val_Lng.Value
                                                              } into removeChars
                                                              select new clsRemoveCharacter
                                                              {
                                                                  ID_RemoveCharacter = removeChars.Key.ID_RemoveCharacter,
                                                                  Name_RemoveCharacter = removeChars.Key.Name_RemoveCharacter,
                                                                  AtBegin = removeChars.Key.AtBegin,
                                                                  Count = removeChars.Key.Count,
                                                                  Offset = removeChars.Key.Offset
                                                              }).ToList();
                                         

            return variableToField;
        }


        public clsDataWork_TextPOrReportsToCommandLine(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        public clsOntologyItem GetData_CmdlrTextParserReport(clsOntologyItem OItem_TextParserOrReport, bool forReports=true)
        {
            this.forReports = forReports;
            objOItem_TextParserOrReport = OItem_TextParserOrReport;
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();
            GetSubData_001_CommandLineToReportOrTestP();
            objOItem_Result = OItem_Result_CommandLineToReportOrTextP;
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                GetSubData_002_CommandLineRun();
                objOItem_Result = OItem_Result_CommandLineRun;

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    GetSubData_003_VarToValue();
                    objOItem_Result = OItem_Result_VarToField;
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        GetSubData_004_VarToValue_FieldAndValue();
                        objOItem_Result = OItem_Result_FieldAndValue;
                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            GetSubData_005_RemoveCharacters();
                            objOItem_Result = OItem_Result_RemoveCharacters;
                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                GetSubData_006_RemoveCharactersAttributes();
                                objOItem_Result = OItem_Result_RemoveCharacters_Attributes;
                                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    GetSubData_007_FieldReplace();
                                    objOItem_Result = OItem_Result_FieldReplace;
                                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                    {
                                        GetSubData_008_FieldReplaceText();
                                        objOItem_Result = OItem_Result_FieldReplaceText;
                                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                        {
                                            GetSubData_009_FieldReplaceToRegex();
                                            objOItem_Result = OItem_Result_RegularExpressions;
                                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                            {
                                                GetSubData_010_Regex();
                                                objOItem_Result = OItem_Result_RegEx;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            

            return objOItem_Result;

        }

        public void GetSubData_001_CommandLineToReportOrTestP()
        {
            OItem_Result_CommandLineToReportOrTextP = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchCommandLineToReport = new List<clsObjectRel> { new clsObjectRel {ID_Other = objOItem_TextParserOrReport.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Object = forReports ? objLocalConfig.OItem_class_comand_line__run__to_report.GUID : objLocalConfig.OItem_class_comand_line__run__to_textparser.GUID } };

            objOItem_Result = objDBLevel_CommandLineToTextPOrReport.GetDataObjectRel(searchCommandLineToReport, doIds: false);

            OItem_Result_CommandLineToReportOrTextP = objOItem_Result;
        }

        public void GetSubData_002_CommandLineRun()
        {
            OItem_Result_CommandLineRun = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchCommandLineRun = objDBLevel_CommandLineToTextPOrReport.ObjectRels.Select(cmdltr => new clsObjectRel
                {
                    ID_Object = cmdltr.ID_Object,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_comand_line__run_.GUID
                }).ToList();

            if (searchCommandLineRun.Any())
            {
                objOItem_Result = objDBLevel_CommandLineRun.GetDataObjectRel(searchCommandLineRun, doIds: false);
            }
            else
            {
                objDBLevel_CommandLineRun.ObjectRels.Clear();
            }

            OItem_Result_CommandLineRun = objOItem_Result;

        }

        public void GetSubData_003_VarToValue()
        {
            OItem_Result_VarToField = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchVarToValue = objDBLevel_CommandLineToTextPOrReport.ObjectRels.Select(cmdltr => new clsObjectRel
                {
                    ID_Other = cmdltr.ID_Object,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                    ID_Parent_Object = forReports ? objLocalConfig.OItem_class_variable_to_field.GUID : objLocalConfig.OItem_class_variable_to_textparserfield.GUID
                }).ToList();

            if (searchVarToValue.Any())
            {
                objOItem_Result = objDBLevel_VarToField.GetDataObjectRel(searchVarToValue, doIds: false);
            }
            else
            {
                objDBLevel_VarToField.ObjectRels.Clear();
            }

            OItem_Result_VarToField = objOItem_Result;
        }

        public void GetSubData_004_VarToValue_FieldAndValue()
        {
            OItem_Result_FieldAndValue = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_VarToValueReportFieldAndValue = objDBLevel_VarToField.ObjectRels.Select(vartfield => new clsObjectRel
                {
                   ID_Object = vartfield.ID_Object,
                   ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                   ID_Parent_Other = forReports ? objLocalConfig.OItem_class_report_field.GUID : objLocalConfig.OItem_class_field.GUID
                }).ToList();

            search_VarToValueReportFieldAndValue.AddRange(objDBLevel_VarToField.ObjectRels.Select(vartfield => new clsObjectRel
            {
                ID_Object = vartfield.ID_Object,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_variable.GUID
            }));

            if (search_VarToValueReportFieldAndValue.Any())
            {
                objOItem_Result = objDBLevel_VarToField_FieldAndValue.GetDataObjectRel(search_VarToValueReportFieldAndValue, doIds: false);
            }
            else
            {
                objDBLevel_VarToField_FieldAndValue.ObjectRels.Clear();
            }

            OItem_Result_FieldAndValue = objOItem_Result;
        }

        public void GetSubData_005_RemoveCharacters()
        {
            OItem_Result_RemoveCharacters = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_RemoveCharacters = objDBLevel_VarToField.ObjectRels.Select(vartfield => new clsObjectRel
            {
                ID_Object = vartfield.ID_Object,
                ID_RelationType = objLocalConfig.OItem_relationtype_uses.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_remove_characters.GUID
            }).ToList();

            search_RemoveCharacters.AddRange(objDBLevel_CommandLineToTextPOrReport.ObjectRels.Select(cmdLineR => new clsObjectRel
            {
                ID_Object = cmdLineR.ID_Object,
                ID_RelationType = objLocalConfig.OItem_relationtype_uses.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_remove_characters.GUID
            }));

            
            if (search_RemoveCharacters.Any())
            {
                objOItem_Result = objDBLevel_RemoveCharacters.GetDataObjectRel(search_RemoveCharacters, doIds: false);
            }
            else
            {
                objDBLevel_RemoveCharacters.ObjectRels.Clear();
            }

            OItem_Result_RemoveCharacters = objOItem_Result;
        }

        public void GetSubData_006_RemoveCharactersAttributes()
        {
            OItem_Result_RemoveCharacters_Attributes = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_RemoveCharacters_Attributes = objDBLevel_RemoveCharacters.ObjectRels.Select(removeChar => new clsObjectAtt
            {
                ID_Object = removeChar.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_at_begin.GUID
            }).ToList();

            search_RemoveCharacters_Attributes.AddRange(objDBLevel_RemoveCharacters.ObjectRels.Select(removeChar => new clsObjectAtt
            {
                ID_Object = removeChar.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_count.GUID
            }));

            search_RemoveCharacters_Attributes.AddRange(objDBLevel_RemoveCharacters.ObjectRels.Select(removeChar => new clsObjectAtt
            {
                ID_Object = removeChar.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_offset.GUID
            }));

            if (search_RemoveCharacters_Attributes.Any())
            {
                objOItem_Result = objDBLevel_RemoveCharacters_Attributes.GetDataObjectAtt(search_RemoveCharacters_Attributes, doIds: false);
            }
            else
            {
                objDBLevel_RemoveCharacters_Attributes.ObjAtts.Clear();
            }

            OItem_Result_RemoveCharacters_Attributes = objOItem_Result;
        }

        public void GetSubData_007_FieldReplace()
        {
            OItem_Result_FieldReplace = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_FieldReplace = objDBLevel_VarToField.ObjectRels.Select(vartfield => new clsObjectRel
            {
                ID_Object = vartfield.ID_Object,
                ID_RelationType = objLocalConfig.OItem_relationtype_uses.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_field_replace__textparser_.GUID
            }).ToList();


            if (search_FieldReplace.Any())
            {
                objOItem_Result = objDBLevel_FieldReplace.GetDataObjectRel(search_FieldReplace, doIds: false);
            }
            else
            {
                objDBLevel_FieldReplace.ObjectRels.Clear();
            }

            OItem_Result_FieldReplace = objOItem_Result;
        }

        public void GetSubData_008_FieldReplaceText()
        {
            OItem_Result_FieldReplaceText = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_FieldReplaceText = objDBLevel_FieldReplace.ObjectRels.Select(fieldReplace => new clsObjectAtt
            {
                ID_Object = fieldReplace.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_text.GUID
            }).ToList();

            if (search_FieldReplaceText.Any())
            {
                objOItem_Result = objDBLevel_FieldReplaceText.GetDataObjectAtt(search_FieldReplaceText, doIds: false);
            }
            else
            {
                objDBLevel_FieldReplaceText.ObjAtts.Clear();
            }

            OItem_Result_FieldReplaceText = objOItem_Result;
        }

        public void GetSubData_009_FieldReplaceToRegex()
        {
            OItem_Result_RegularExpressions = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_RegularExpressions = objDBLevel_FieldReplace.ObjectRels.Select(fieldReplace => new clsObjectRel
            {
                ID_Object = fieldReplace.ID_Other,
                ID_RelationType = objLocalConfig.OItem_relationtype_find.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_regular_expressions.GUID
            }).ToList();


            if (search_RegularExpressions.Any())
            {
                objOItem_Result = objDBLevel_RegularExpressions.GetDataObjectRel(search_RegularExpressions, doIds: false);
            }
            else
            {
                objDBLevel_RegularExpressions.ObjectRels.Clear();
            }

            OItem_Result_RegularExpressions = objOItem_Result;
        }

        public void GetSubData_010_Regex()
        {
            OItem_Result_RegEx = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var search_RegEx = objDBLevel_RegularExpressions.ObjectRels.Select(regularExpression => new clsObjectAtt
            {
                ID_Object = regularExpression.ID_Other,
                ID_AttributeType = objLocalConfig.OItem_attributetype_regex.GUID
            }).ToList();

            if (search_RegEx.Any())
            {
                objOItem_Result = objDBLevel_RegEx.GetDataObjectAtt(search_RegEx, doIds: false);
            }
            else
            {
                objDBLevel_RegEx.ObjAtts.Clear();
            }

            OItem_Result_RegEx = objOItem_Result;
        }

        private void Initialize()
        {

            objDBLevel_CommandLineToTextPOrReport = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_CommandLineRun = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_VarToField = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_VarToField_FieldAndValue = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RemoveCharacters = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RemoveCharacters_Attributes = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldReplace = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_FieldReplaceText = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegularExpressions = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RegEx = new OntologyModDBConnector(objLocalConfig.Globals);

            OItem_Result_CommandLineToReportOrTextP = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_CommandLineRun = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_VarToField = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_FieldAndValue = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_RemoveCharacters = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_RemoveCharacters_Attributes = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_FieldReplace = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_FieldReplaceText = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_RegularExpressions = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_RegEx = objLocalConfig.Globals.LState_Nothing.Clone();
    }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public class CommandLineRunTreeItem
    {
        public string IdCommandLineRun { get; set; }
        public string NameCommandLineRun { get; set; }
        public object TreeNodeItem { get; set; }
        public bool IsRootNode { get; set; }
    }
}

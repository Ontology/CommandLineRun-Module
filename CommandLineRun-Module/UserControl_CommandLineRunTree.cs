﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using ClassLibrary_ShellWork;
using CommandLineRun_Module.Converter;

namespace CommandLineRun_Module
{
    public partial class UserControl_CommandLineRunTree : UserControl
    {
        private CommandLineRunTree_WinformsViewModel viewModel;

        private clsModuleSelectionWorker moduleSelectionWorker = new clsModuleSelectionWorker();

        private delegate void viewModelChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e);

        public delegate void SelectedNode(TreeNode selectedNode);
        public delegate void ClearTreeNodes();

        private frm_ObjectEdit objFrmObjectEdit;

        public event SelectedNode selectedNode;

        private frmModules objFrmModules;
        private frmAdvancedFilter objFrmAdvancedFilter;
        private frmVariableValueMapper objFrmVariableValueMapper;

       

        private TreeNode treeNode_Root;

        public clsOntologyItem OItem_CommandLineRun
        {
            get { return viewModel.OItemApplied; }
        }

        public delegate void AppliedCommandLineRun();

        public event AppliedCommandLineRun appliedCommandLineRun;

        public bool Applyable
        {
            get { return applyToolStripMenuItem.Visible; }
            set { applyToolStripMenuItem.Visible = value; }
        }

        public UserControl_CommandLineRunTree(clsLocalConfig LocalConfig, clsDataWork_CommandLineRun DataWork_CommandLineRun)
        {
            InitializeComponent();
            viewModel = new CommandLineRunTree_WinformsViewModel(LocalConfig, DataWork_CommandLineRun);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            viewModel.clearCommandLineTreeNode += ViewModel_clearCommandLineTreeNode;
            viewModel.IntializeForm();
            
            
        }

        private void ViewModel_clearCommandLineTreeNode()
        {
            if (this.InvokeRequired)
            {
                var delegateItem = new ClearTreeNodes(ViewModel_clearCommandLineTreeNode);
                this.Invoke(delegateItem);
            }
            else
            {
                treeView_CMDLR.Nodes.Clear();
            }
            
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var delegPropertyChange = new viewModelChanged(ViewModel_PropertyChanged);
                this.Invoke(delegPropertyChange, sender, e);
            }
            else
            {  
                if (e.PropertyName == NotifyChanges.CommandLineRunTree_IsApplyable)
                {
                    applyToolStripMenuItem.Visible = viewModel.IsApplyable;
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_TreeItemRoot)
                {
                    treeView_CMDLR.Nodes.Add((TreeNode)viewModel.TreeItemRoot.TreeNodeItem);
                    
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_TreeNodeCount)
                {
                    toolStripLabel_Count.Text = viewModel.TreeNodeCount;
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_TreeItemsToUnmark)
                {
                    viewModel.TreeItemsToUnmark.ForEach(treeItem =>
                    {
                        ((TreeNode)treeItem.TreeNodeItem).BackColor = viewModel.NodeBackColor;
                    });
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_TreeItemsToMark)
                {
                    viewModel.TreeItemsToMark.ForEach(treeItem =>
                    {
                        ((TreeNode)treeItem.TreeNodeItem).BackColor = viewModel.NodeBackColor_marked;
                    });
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_SelectedTreeItem)
                {


                    treeView_CMDLR.SelectedNode = (TreeNode)viewModel.SelectedTreeItem.TreeNodeItem;
                    if (treeView_CMDLR.SelectedNode!= null && (treeView_CMDLR.SelectedNode.FullPath != viewModel.LastTreeNodeFullPath || viewModel.RefreshItem) && selectedNode != null)
                    {
                        viewModel.LastTreeNodeFullPath = treeView_CMDLR.SelectedNode.FullPath;
                        selectedNode(treeView_CMDLR.SelectedNode);
                    }
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_RefreshItem)
                {
                    if (viewModel.RefreshItem && treeView_CMDLR.SelectedNode != null)
                    {
                        viewModel.RefreshItem = false;
                        selectedNode(treeView_CMDLR.SelectedNode);
                    }
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_ObjectsToEdit)
                {
                    objFrmObjectEdit = new frm_ObjectEdit(viewModel.LocalConfig.Globals, viewModel.ObjectsToEdit, 0, viewModel.LocalConfig.Globals.Type_Object, null);
                    objFrmObjectEdit.ShowDialog(this);
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_OItemToSendToModule)
                {
                    moduleSelectionWorker.OpenLastModule = OpenLastModuleToolStripMenuItem.Checked;
                    moduleSelectionWorker.LastModuleName = OpenLastModuleToolStripMenuItem.ToolTipText;
                    var result =  moduleSelectionWorker.OpenModuleWithArgument(viewModel.OItemToSendToModule, this, viewModel.LocalConfig.Globals);
                    if (result.GUID == viewModel.LocalConfig.Globals.LState_Success.GUID)
                    {
                        OpenLastModuleToolStripMenuItem.ToolTipText = moduleSelectionWorker.LastModuleName;
                    }
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_Result_SemFilter)
                {
                    if (viewModel.Result_SemFilter.GUID == viewModel.LocalConfig.Globals.LState_Success.GUID)
                    {
                        viewModel.FillTree();
                    }
                }                
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_SemFilter)
                {
                    toolStripTextBox_SemFilter.Text = viewModel.SemFilter;
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_OItemApplied)
                {
                    if (viewModel.OItemApplied != null && appliedCommandLineRun != null)
                    {
                        appliedCommandLineRun();
                    }
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_TreeItemsToExpandParents)
                {
                    viewModel.TreeItemsToExpandParents.ForEach(treeItem =>
                    {
                        var parentNodeTmp = ((TreeNode)treeItem.TreeNodeItem).Parent;
                        while (parentNodeTmp != null)
                        {
                            parentNodeTmp.Expand();
                            parentNodeTmp = parentNodeTmp.Parent;
                        }
                    });
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_OItemToMapVarToVal)
                {
                    objFrmVariableValueMapper = new frmVariableValueMapper(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun, viewModel.OItemToMapVarToVal);
                    objFrmVariableValueMapper.ShowDialog(this);

                    viewModel.RefreshCodes();
                }
                else if (e.PropertyName == NotifyChanges.CommandLineRunTree_FormMessage)
                {
                    MessageBox.Show(this, viewModel.FormMessage.Message, viewModel.FormMessage.Short, FormMessageToButtonConverter.Convert(viewModel.FormMessage), FormMessageToIconConverter.Convert(viewModel.FormMessage));
                }

            }
        }

        public void InitializeTree()
        {
            viewModel.FillTree();
        }

        private void treeView_CMDLR_AfterSelect(object sender, TreeViewEventArgs e)
        {
            viewModel.SelectNode(e.Node.Name);
        }

        //private void ShowCMDLR_Data(TreeNode node)
        //{
        //    if (!searchIsRunning)
        //    {
        //        selectedNode(node);
        //    }
        //}

        private void toolStripTextBox_Mark_TextChanged(object sender, EventArgs e)
        {
            viewModel.ResetMarkTimer();
        }

        //private void timer_Mark_Tick(object sender, EventArgs e)
        //{
            
        //    if (treeView_CMDLR.SelectedNode != null)
        //    {

        //        ShowCMDLR_Data(treeView_CMDLR.SelectedNode);
        //    }
        //    else
        //    {
                
        //        treeView_CMDLR.SelectedNode = treeNode_Root;
        //    }
        //}

        //private void UnmarkNodes(TreeNode parentNode = null)
        //{
        //    if (parentNode == null)
        //    {
        //        foreach (TreeNode subNode in treeView_CMDLR.Nodes)
        //        {
        //            subNode.BackColor = nodeBackColor;

        //            UnmarkNodes(subNode);
        //        }
        //    }
        //    else
        //    {
        //        foreach (TreeNode subNode in parentNode.Nodes)
        //        {
        //            subNode.BackColor = nodeBackColor;

        //            UnmarkNodes(subNode);
        //        }
        //    }
        //}

        //private void SearchNode(string search, bool isGuid, TreeNode parentNode = null)
        //{
            
        //    if (parentNode == null)
        //    {
                
        //        foreach (TreeNode subNode in treeView_CMDLR.Nodes)
        //        {
        //            if (isGuid)
        //            {
        //                if (subNode.Name == search)
        //                {
        //                    subNode.BackColor = nodeBackColor_marked;
                            
        //                    var parentNodeTmp = subNode.Parent;
        //                    while (parentNodeTmp != null)
        //                    {
        //                        parentNodeTmp.Expand();
        //                        parentNodeTmp = parentNodeTmp.Parent;
        //                    }

                            
        //                }
        //            }
        //            else
        //            {
        //                if (subNode.Text.ToLower().Contains(search.ToLower()))
        //                {
        //                    subNode.BackColor = nodeBackColor_marked;
                            
        //                    var parentNodeTmp = subNode.Parent;
        //                    while (parentNodeTmp != null)
        //                    {
        //                        parentNodeTmp.Expand();
        //                        parentNodeTmp = parentNodeTmp.Parent;
        //                    }

        //                }
        //            }

        //            SearchNode(search, isGuid, subNode);

        //        }
        //    }
        //    else
        //    {
        //        foreach (TreeNode subNode in parentNode.Nodes)
        //        {
        //            if (isGuid)
        //            {
        //                if (subNode.Name == search)
        //                {
        //                    subNode.BackColor = nodeBackColor_marked;
                            
        //                    var parentNodeTmp = subNode.Parent;
        //                    while (parentNodeTmp != null)
        //                    {
        //                        parentNodeTmp.Expand();
        //                        parentNodeTmp = parentNodeTmp.Parent;
        //                    }
                            
                            
        //                }
        //            }
        //            else
        //            {
        //                if (subNode.Text.ToLower().Contains(search.ToLower()))
        //                {
        //                    subNode.BackColor = nodeBackColor_marked;
                            
        //                    var parentNodeTmp = subNode.Parent;
        //                    while (parentNodeTmp != null)
        //                    {
        //                        parentNodeTmp.Expand();
        //                        parentNodeTmp = parentNodeTmp.Parent;
        //                    }
                            
                            
        //                }
        //            }

        //            SearchNode(search, isGuid, subNode);

        //        }
        //    }
        //}

        private void treeView_CMDLR_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            viewModel.TreeItemDoubleClick(e.Node.Name);
        }

        private void contextMenuStrip_CMDLR_Opening(object sender, CancelEventArgs e)
        {
            var node = treeView_CMDLR.SelectedNode;

            viewModel.ReconfigureEnableStates(node.Name);
        }

        private void OpenModuleByArgumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = treeView_CMDLR.SelectedNode;
            viewModel.SendItemToModule(node.Name);
        }

        private void toolStripButton_AddSemFilter_Click(object sender, EventArgs e)
        {
            objFrmAdvancedFilter = new frmAdvancedFilter(viewModel.LocalConfig.Globals, viewModel.LocalConfig.OItem_class_comand_line__run_);
            objFrmAdvancedFilter.ShowDialog(this);

            if (objFrmAdvancedFilter.DialogResult == DialogResult.OK)
            {
                viewModel.AddSemFilter(objFrmAdvancedFilter.OItem_Class, objFrmAdvancedFilter.OItem_Direction, objFrmAdvancedFilter.OItem_RelationType, objFrmAdvancedFilter.OItem_Object);
               

            }
        }

        private void applyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = treeView_CMDLR.SelectedNode;

            if (node != null)
            {
                viewModel.AppliedNode(node.Name);
            }
            

            
        }

        public void MarkNodes(List<clsOntologyItem> commandLineRunList)
        {
            viewModel.MarkNodesByList(commandLineRunList);
        }

        public void SelectNode(clsOntologyItem commandLineRun)
        {
            var treeNode = treeView_CMDLR.Nodes.Find(commandLineRun.GUID, true).FirstOrDefault();
            if (treeNode != null)
            {
                treeView_CMDLR.SelectedNode = treeNode;
            }
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = treeView_CMDLR.SelectedNode;
            
            if (node != null)
            {
                viewModel.RefreshTreeItem(node.Name);
            }
        }

        private void treeView_CMDLR_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    viewModel.FillTree();
                    break;
            }
        }

        private void mapVariablesToValuesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = treeView_CMDLR.SelectedNode;
            if (node != null)
            {
                viewModel.SetMapVariableToValueItem(node.Name);
            }
            
        }

    }
}

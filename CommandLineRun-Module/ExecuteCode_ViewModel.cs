﻿using CommandLineRun_Module.BaseClasses;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public delegate void ScriptExecuted(string output, string error);

    public class ExecuteCode_ViewModel : ViewModelBase
    {
        protected virtual void OnScriptExecuted(string output, string error)
        {
            ScriptExecuted handler = scriptExecuted;
            if (handler != null) handler(output, error);
        }

        public event ScriptExecuted scriptExecuted;

        public clsLocalConfig LocalConfig { get; private set; }
        private clsOntologyItem objOItem_CMDLR;
        public clsOntologyItem OItem_CMDLR
        {
            get { return objOItem_CMDLR; }
            set
            {
                objOItem_CMDLR = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_OItem_CMDLR);
            }
        }

        public clsDataWork_CommandLineRun DataWork_CommandLineRun { get; private set; }

        private clsExecutableConfiguration objExecutionConfiguration;
        public clsExecutableConfiguration ExecutionConfiguration
        {
            get { return objExecutionConfiguration; }
            set
            {
                objExecutionConfiguration = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_ExecutionConfiguration);
            }
        }

        private Encoding scriptEncoding;
        public Encoding ScriptEncoding
        {
            get { return scriptEncoding; }
            set
            {
                scriptEncoding = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_ScriptEncoding);
            }
        }

        private List<EncodingInfo> encodings;
        public List<EncodingInfo> Encodings
        {
            get { return encodings; }
            set
            {
                encodings = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_Encodings);
            }
        }

        private EncodingInfo encodingItem;
        public EncodingInfo EncodingItem
        {
            get { return encodingItem; }
            set
            {
                encodingItem = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_Encoding);
            }
        }

        public string EncodingSelect_ValueMember { get { return "CodePage"; } }
        public string EncodingSelect_DisplayMember { get { return "DisplayName"; } }

        private int positionOfWord;
        public int PositionOfWord
        {
            get { return positionOfWord; }
            set
            {
                positionOfWord = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_PositionOfWord);
            }
        }
        private string wordOfPosition;
        public string WordOfPosition
        {
            get { return wordOfPosition; }
            set
            {
                wordOfPosition = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_WordOfPosition);
            }
        }

        private clsOntologyItem objOItem_ProgrammingLanguage;
        public clsOntologyItem OItem_ProgrammingLanguage
        {
            get { return objOItem_ProgrammingLanguage; }
            set
            {
                objOItem_ProgrammingLanguage = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_OItem_ProgrammingLanguage);
            }
        }

        private clsOntologyItem autoCorrectorUsable;
        public clsOntologyItem AutoCorrectorUsable
        {
            get { return autoCorrectorUsable; }
            set
            {
                autoCorrectorUsable = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_AutoCorrectorUsable);
            }
        }

        private bool isCodeReadonly;
        public bool IsCodeReadonly
        {
            get { return isCodeReadonly; }
            set
            {
                isCodeReadonly = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_IsCodeReadonly);
            }
        }

        private bool isCodeParsedReadonly;
        public bool IsCodeParsedReadonly
        {
            get { return isCodeParsedReadonly; }
            set
            {
                isCodeParsedReadonly = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_IsCodeParsedReadonly);
            }
        }

        private string codeText;
        public string CodeText
        {
            get { return codeText; }
            set
            {
                codeText = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_CodeText);
            }
        }

        public string NotifyChange_CodeParsedText
        {
            get { return NotifyChanges.ExecuteCodeViewModel_CodeParsedText; }
        }

        private string codeParsedText;
        public string CodeParsedText
        {
            get { return codeParsedText; }
            set
            {
                codeParsedText = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_CodeParsedText);
            }
        }

        private bool isEnabledExec;
        public bool IsEnabledExec
        {
            get { return isEnabledExec; }
            set
            {
                isEnabledExec = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_IsEnabledExec);
            }
        }

        private bool isEnabledSave;
        public bool IsEnabledSave
        {
            get { return isEnabledSave; }
            set
            {
                isEnabledSave = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_IsEnabledSave);
            }
        }

        private bool isCMDRLReadOnly;
        public bool IsCMDRLReadOnly
        {
            get { return isCMDRLReadOnly; }
            set
            {
                isCMDRLReadOnly = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_IsCMDRLReadOnly);
            }
        }

        private string cMDRLText;
        public string CMDRLText
        {
            get { return cMDRLText; }
            set
            {
                cMDRLText = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_CMDRLText);
            }
        }

        private string programmingLanguageName;
        public string ProgrammingLanguageName
        {
            get { return programmingLanguageName; }
            set
            {
                programmingLanguageName = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_ProgrammingLanguageName);
            }
        }

        private string syntaxHighlighter;
        public string SyntaxHighlighter
        {
            get { return syntaxHighlighter; }
            set
            {
                syntaxHighlighter = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_SyntaxHighlighter);
            }
        }

        private FormMessage formMessage;
        public FormMessage FormMessage
        {
            get { return formMessage; }
            set
            {
                formMessage = value;
                RaisePropertyChanged(NotifyChanges.ExecuteCodeViewModel_FormMessage);
            }
        }

        public void Clear()
        {
            IsCodeReadonly = false;
            IsCodeParsedReadonly = false;
            CodeText = "";
            CodeParsedText = "";
            IsCodeReadonly = true;
            IsCodeParsedReadonly = true;
            IsEnabledExec = false;
            IsEnabledSave = false;

            IsCMDRLReadOnly = true;
            CMDRLText = "";
        }



        public void InitializeCodeView(clsOntologyItem oItem_Cmdrl, bool doExecute = false)
        {
            Clear();
            OItem_CMDLR = oItem_Cmdrl;

            if (objOItem_CMDLR != null && objOItem_CMDLR.GUID != LocalConfig.Globals.Root.GUID)
            {
                CMDRLText = objOItem_CMDLR.Name;
                IsCMDRLReadOnly = false;

                var codes =
                    DataWork_CommandLineRun.Codes.Where(code => code.ID_CommandLineRun == objOItem_CMDLR.GUID)
                                              .ToList();

                var subCmdrls = DataWork_CommandLineRun.GetSubCmdlrs(OItem_CMDLR);

                codes.AddRange(from objCode in DataWork_CommandLineRun.Codes
                               join subCmdrl in subCmdrls on objCode.ID_CommandLineRun equals subCmdrl.ID_Other
                               join codeExist in codes on objCode.ID_CommandLineRun equals codeExist.ID_CommandLineRun into codesExist
                               from codeExist in codesExist.DefaultIfEmpty()
                               where codeExist == null
                               orderby subCmdrl.OrderID
                               select objCode);

                IsCodeReadonly = false;
                IsCodeParsedReadonly = false;

                codes.ForEach(code =>
                {
                    EncodingInfo encTemp = null;
                    if (code.Name_Encoding != null)
                    {

                        encTemp = encodings.Where(enc => enc.DisplayName.ToLower() == code.Name_Encoding.ToLower()).FirstOrDefault();
                        if (encTemp != null)
                        {
                            if (scriptEncoding == null)
                            {
                                scriptEncoding = encTemp.GetEncoding();
                            }
                            else
                            {
                                if (scriptEncoding != encTemp.GetEncoding())
                                {
                                    scriptEncoding = null;
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(CodeText))
                    {
                        CodeText += "\r\n";
                    }
                    if (!string.IsNullOrEmpty(CodeParsedText))
                    {
                        CodeParsedText += "\r\n";
                    }
                    CodeParsedText += code.CodeParsed;
                    CodeText += code.Code;
                });


                var programmingLanguages = (from code in codes
                                            group code by new { code.ID_ProgrammingLanguage, code.Name_ProgrammingLanguage }
                                                into pls
                                            where pls.Key.ID_ProgrammingLanguage != null
                                            select pls.Key).ToList();

                objOItem_ProgrammingLanguage = programmingLanguages.Count == 1 ? new clsOntologyItem
                {
                    GUID = programmingLanguages.First().ID_ProgrammingLanguage,
                    Name = programmingLanguages.First().Name_ProgrammingLanguage,
                    GUID_Parent = LocalConfig.OItem_class_programing_language.GUID,
                    Type = LocalConfig.Globals.Type_Object
                } : null;


                ProgrammingLanguageName = programmingLanguages.Count == 1 ? programmingLanguages.First().Name_ProgrammingLanguage : "";

                if (programmingLanguages.Count == 1)
                {
                    var exeConfig =
                        DataWork_CommandLineRun.ExecutableConfigurations.Where(
                            exec => exec.ID_ProgrammingLanguage == programmingLanguages.First().ID_ProgrammingLanguage)
                                                  .ToList();

                    var syntaxHighlightPL = (from code in codes
                                             group code by code.Name_SyntaxHighlighting
                                               into highlights
                                             where highlights.Key != null
                                             select highlights.Key).ToList();

                    objExecutionConfiguration = exeConfig.FirstOrDefault();

                    if (syntaxHighlightPL.Count == 1 && objExecutionConfiguration != null)
                    {
                        objExecutionConfiguration.Name_SyntaxHighlight = syntaxHighlightPL.First();
                        IsEnabledExec = true;
                    }

                }


                var syntaxHighlight = (from code in codes
                                       group code by code.Name_SyntaxHighlighting
                                       into highlights
                                       where highlights.Key != null
                                       select highlights.Key).ToList();


                SyntaxHighlighter = syntaxHighlight.Count == 1 ? syntaxHighlight.First() : "";

                IsCodeReadonly = true;
                IsCodeParsedReadonly = true;




                IsEnabledSave = true;



                if (scriptEncoding != null)
                {
                    EncodingItem = Encodings.FirstOrDefault(enc => enc.CodePage == scriptEncoding.CodePage);
                }
            }

        }

        public ExecuteCode_ViewModel(clsLocalConfig localConfig, clsDataWork_CommandLineRun dataWork_CommandLineRun)
        {
            LocalConfig = localConfig;
            DataWork_CommandLineRun = dataWork_CommandLineRun;
        }

        public ExecuteCode_ViewModel(Globals globals)
        {
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }


            DataWork_CommandLineRun = new clsDataWork_CommandLineRun(LocalConfig);
            var objOItem_Result = DataWork_CommandLineRun.GetData_CommandLineRun();
            if (objOItem_Result.GUID == LocalConfig.Globals.LState_Error.GUID)
            {
                throw new Exception("Command Line Run cannot be determined!");
            }
        }

        public void InitViewModel()
        {
            var objOItem_Result = DataWork_CommandLineRun.GetData_ExecutableConfiguration();

            if (objOItem_Result.GUID == LocalConfig.Globals.LState_Error.GUID)
            {
                throw new Exception("Executable-Configuration Error");
            }

            Encodings = Encoding.GetEncodings().OrderBy(enc => enc.DisplayName).ToList();



        }
        
    }
}

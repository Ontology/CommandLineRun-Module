﻿using CommandLineRun_Module.BaseClasses;
using Ontology_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    [Flags]
    public enum ViewModelError
    {
        None = 0,
        ReportsToCommandLineInstance = 1
    }
    public enum ViewModelType
    {
        Standard = 1,
        Report = 2,
        CommandLineAndShellOutput = 4

    }

    public enum CloseType
    {
        None = 0,
        OK = 1,
        Cancel = 2
    }

    public enum TimerActionType
    {
        Stop = 0,
        Start = 1
    }

    public delegate void CloseForm(CloseType closeType);

    public delegate void AppliedCommandLineRun();

    public class frmCommandLineRun_ViewModel : ViewModelBase
    {
        public event CloseForm closeForm;

        public clsLocalConfig LocalConfig { get; set; }

        private clsDataWork_CommandLineRun objDataWork_CommandLineRun;
        public clsDataWork_CommandLineRun DataWork_CommandLineRun
        {
            get { return objDataWork_CommandLineRun; }
            set
            {
                objDataWork_CommandLineRun = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_DataWork_CommandLineRun);
            }
        }

        private clsDataWork_TextPOrReportsToCommandLine objDataWork_ReportsToCommandLine;
        public clsDataWork_TextPOrReportsToCommandLine DataWork_ReportsToCommandLine
        {
            get { return objDataWork_ReportsToCommandLine; }
            set
            {
                objDataWork_ReportsToCommandLine = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_DataWork_ReportsToCommandLine);
            }
        }
        private clsArgumentParsing objArgumentParsing;

        private clsModuleFunction objModuleFunction;

        private string[] strCommandLine;

        private clsShellOutput objShellOutput;

        private clsOntologyItem objOItem_Report;
        public clsOntologyItem Report
        {
            get { return objOItem_Report; }
            set
            {
                objOItem_Report = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_Report);
            }
        }
        private clsOntologyItem objOItem_TextParser;
        public clsOntologyItem TextParser
        {
            get { return objOItem_TextParser; }
            set
            {
                objOItem_TextParser = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_TextParser);
            }
        }

        public event AppliedCommandLineRun appliedItem;

        private List<KeyValuePair<string, string>> columnFieldList;
        public List<KeyValuePair<string, string>> ColumnFieldList
        {
            get { return columnFieldList; }
            set
            {
                columnFieldList = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_ColumnFieldList);
            }
        }

        private bool reportRegistered;
        public bool ReportRegistered
        {
            get { return reportRegistered; }
            set
            {
                reportRegistered = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_ReportRegistered);
            }
        }

        private bool initializeReportsOrTextParser = false;
        public bool InitializeReportsOrTextParser
        {
            get { return initializeReportsOrTextParser; }
            set
            {
                initializeReportsOrTextParser = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_InitializeReportsOrTextParser);
            }
        }
            
        private bool forReports;

        private bool applyable;
        public bool Applyable
        {
            get { return applyable; }
            set
            {
                applyable = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_Applyable);
            }
        }

        private bool autoRun = false;
        public bool AutoRun
        {
            get { return autoRun; }
            set
            {
                autoRun = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_AutoRun);
            }
        }

        private string formHeaderText;
        public string FormHeaderText
        {
            get { return formHeaderText; }
            set
            {
                formHeaderText = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_FormHeaderText);
            }
        }

        private ViewModelError viewModelError;
        public ViewModelError ViewModelError
        {
            get { return viewModelError; }
            set
            {
                viewModelError = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_ViewModelError);
            }
        }

        private ViewModelType viewModelType;
        public ViewModelType ViewModelType
        {
            get { return viewModelType; }
            set
            {
                viewModelType = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_ViewModelType);
            }
        }

        private bool commandLineRunDetermined;
        public bool CommandLineRunDetermined
        {
            get { return commandLineRunDetermined; }
            set
            {
                commandLineRunDetermined = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_CommandLineRunDetermined);
            }
        }

        private bool showSnippletEditor;
        public bool ShowSnippletEditor
        {
            get { return showSnippletEditor; }
            set
            {
                showSnippletEditor = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_ShowSnippletEditor);
            }
        }

        private clsOntologyItem codeSnipplet;
        public clsOntologyItem CodeSnipplet
        {
            get { return codeSnipplet; }
            set
            {
                codeSnipplet = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRun_CodeSnipplet);
            }
        }

        private string registeredReportName;
        public string RegisteredReportName
        {
            get { return registeredReportName; }
            set
            {
                registeredReportName = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRunWinForms_RegisteredReportName);
            }
        }

        private TimerActionType timerActionType;
        public TimerActionType TimerActionType
        {
            get { return timerActionType; }
            set
            {
                timerActionType = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRunWinForms_TimerActionType);
            }
        }

        private CodeViewInit codeViewInit;
        public CodeViewInit CodeViewInit
        {
            get { return codeViewInit; }
            set
            {
                codeViewInit = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRunWinForms_CodeViewInit);
            }
        }

        private FormMessage formMessage;
        public FormMessage FormMessage
        {
            get { return formMessage; }
            set
            {
                formMessage = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRunWinForms_FormMessage);
            }
        }

        public frmCommandLineRun_ViewModel(Globals globals)
        {
            strCommandLine = null;
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }
            objDataWork_CommandLineRun = new clsDataWork_CommandLineRun(LocalConfig);
        }

        public frmCommandLineRun_ViewModel()
        {
            strCommandLine = null;
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }
            objDataWork_CommandLineRun = new clsDataWork_CommandLineRun(LocalConfig);
        }

        private void TestReports()
        {
            objOItem_Report = new clsOntologyItem
            {
                GUID = "c0e32894ee38457490e03d44efc3fdca",
                Name = "Servers with IP-Address and Alias",
                GUID_Parent = "30cbc6e89c0f47d6920c97fdc40ea1de",
                Type = LocalConfig.Globals.Type_Object
            };

            strCommandLine = null;
            Initialize_TextParserOrReport();
        }

        public void InitForm()
        {
            //TestReports();
            
            
            ViewModelType = ViewModelType.Standard;
            ParseArguments();
            if (initializeReportsOrTextParser)
            {
                Initialize_TextParserOrReport();
            }
            else
            {
                Initialize();
            }
            
        }

        public void ScriptExecuted(string output, string error)
        {
            objShellOutput.ErrorText = error;
            objShellOutput.OutputText = output;
        }

        public void InitFormCommandLineAndShellOutput(string[] commandLine, clsShellOutput shellOutput)
        {
            ViewModelType = ViewModelType.CommandLineAndShellOutput;
            objShellOutput = shellOutput;
            strCommandLine = commandLine;
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }
            DataWork_CommandLineRun = new clsDataWork_CommandLineRun(LocalConfig);
            ParseArguments();

            if (initializeReportsOrTextParser)
            {
                Initialize_TextParserOrReport();
            }
            else
            {
                Initialize();
            }

            
        }

        public void InitFormReport(clsOntologyItem OItem_TextParserOrReport, bool forReports = true)
        {
            ViewModelType = ViewModelType.Report;
            strCommandLine = null;
            this.forReports = forReports;

            objOItem_Report = OItem_TextParserOrReport;
            Initialize_TextParserOrReport();

            
        }

        private void Initialize()
        {

            if (LocalConfig.objOItem_Session != null)
            {
                Applyable = true;
                
            }

            var objOItem_Result = objDataWork_CommandLineRun.GetData_CommandLineRun();

            var autoRun = false;
            if (objOItem_Result.GUID == LocalConfig.Globals.LState_Success.GUID)
            {
                if (objModuleFunction != null)
                {
                    if (objModuleFunction.Name_Function.ToLower() == "execute" && objDataWork_CommandLineRun.OItem_Object != null && objDataWork_CommandLineRun.OItem_Object.GUID_Parent == LocalConfig.OItem_class_comand_line__run_.GUID)
                    {
                        autoRun = true;
                    }
                }


            }

            AutoRun = autoRun;
        }

        private void Initialize_TextParserOrReport()
        {
            FormHeaderText = forReports ? "Report: " : "TextParser: " + objOItem_Report.Name;

            DataWork_ReportsToCommandLine = new clsDataWork_TextPOrReportsToCommandLine(LocalConfig);
            var objOItem_Result = DataWork_ReportsToCommandLine.GetData_CmdlrTextParserReport(objOItem_Report, forReports);
            if (objOItem_Result.GUID == LocalConfig.Globals.LState_Success.GUID)
            {
                if (DataWork_CommandLineRun == null) DataWork_CommandLineRun = new clsDataWork_CommandLineRun(LocalConfig);
                

                objOItem_Result = DataWork_CommandLineRun.GetData_CommandLineRun();

                if (objOItem_Result.GUID == LocalConfig.Globals.LState_Success.GUID)
                {
                    CommandLineRunDetermined = true;
                    
                }
                else
                {
                    CommandLineRunDetermined = false;
                }
            }
            else
            {
                ViewModelError |= ViewModelError.ReportsToCommandLineInstance;
            }

        }

        private void ParseArguments()
        {

            if (strCommandLine == null)
            {
                objArgumentParsing = new clsArgumentParsing(LocalConfig.Globals,
                                                            new List<string>(Environment.GetCommandLineArgs()));
            }
            else
            {
                objArgumentParsing = new clsArgumentParsing(LocalConfig.Globals,
                                                            new List<string>(strCommandLine));
            }


            if (objArgumentParsing.OList_Items.Count == 1 || !string.IsNullOrEmpty(objArgumentParsing.Session))
            {
                clsOntologyItem objOItem_Argument = null;

                if (objArgumentParsing.OList_Items.Count == 1)
                {
                    objOItem_Argument = objArgumentParsing.OList_Items[0];
                }
                else if (!string.IsNullOrEmpty(objArgumentParsing.Session))
                {
                    LocalConfig.objOItem_Session = objDataWork_CommandLineRun.GetOItem(objArgumentParsing.Session, LocalConfig.Globals.Type_Object);
                    if (LocalConfig.objOItem_Session != null)
                    {
                        LocalConfig.objSession = new clsSession(LocalConfig.Globals);
                        var objOList_SessionItems = LocalConfig.objSession.GetItems(LocalConfig.objOItem_Session, true);
                        if (objOList_SessionItems.Count > 0)
                        {
                            objOItem_Argument = objOList_SessionItems[0];
                        }
                    }
                }


                if (objOItem_Argument != null)
                {
                    objModuleFunction = objArgumentParsing.FunctionList != null ? objArgumentParsing.FunctionList.Count == 1 ? objArgumentParsing.FunctionList[0] : null : null;

                    if (objOItem_Argument.Type.ToLower() == LocalConfig.Globals.Type_Class.ToLower())
                    {
                        objOItem_Argument.Type = LocalConfig.Globals.Type_Class;
                        objDataWork_CommandLineRun.OItem_Class = objOItem_Argument;
                        FormHeaderText = objOItem_Argument.Name;
                    }
                    else if (objOItem_Argument.Type.ToLower() == LocalConfig.Globals.Type_RelationType.ToLower())
                    {
                        objOItem_Argument.Type = LocalConfig.Globals.Type_RelationType;
                        objDataWork_CommandLineRun.OItem_RelationType = objOItem_Argument;
                        FormHeaderText = objOItem_Argument.Name;
                    }
                    else if (objOItem_Argument.Type.ToLower() == LocalConfig.Globals.Type_Object.ToLower())
                    {
                        if (objOItem_Argument.GUID_Parent == LocalConfig.OItem_class_code_snipplets.GUID)
                        {
                            CodeSnipplet = objOItem_Argument;
                            ShowSnippletEditor = true;
                            
                        }
                        else
                        {
                            objOItem_Argument.Type = LocalConfig.Globals.Type_Object;
                            objDataWork_CommandLineRun.OItem_Object = objOItem_Argument;

                            var objOItem_Class =
                                    objDataWork_CommandLineRun.GetOItem(objDataWork_CommandLineRun.OItem_Object.GUID_Parent,
                                                                        LocalConfig.Globals.Type_Class);


                            FormHeaderText = objOItem_Class.Name + "/" + objOItem_Argument.Name;
                            
                            if (objArgumentParsing.FunctionList != null && objArgumentParsing.FunctionList.Count > 0)
                            {
                                if (objArgumentParsing.FunctionList[0].GUID_Function == LocalConfig.OItem_object_commandlinerun_module.GUID)
                                {
                                    InitializeReportsOrTextParser = true;
                                    Report = objOItem_Argument;
                                }
                            }
                        }


                    }
                }

            }
        }

        public void CommandLineRunApplied()
        {
            if (LocalConfig.objOItem_Session != null && Report == null)
            {
                if (closeForm != null)
                {
                    closeForm(CloseType.OK);
                }
            }
            else
            {

                TimerActionType = TimerActionType.Stop;
                if (LocalConfig.objOItem_Session == null)
                {
                    if (appliedItem != null)
                    {
                        appliedItem();
                    }
                    
                }
                else
                {
                    LocalConfig.objSession.FinishActor(LocalConfig.objOItem_Session);
                    TimerActionType = TimerActionType.Start;
                }

            }
        }

        public void SessionTick()
        {
            var formMessage = new FormMessage("Beim Datenaustausch sind Fehler aufgetreten!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
            if (LocalConfig.objOItem_Session != null)
            {
                var objOItem_Result = LocalConfig.objSession.InitiatorFinished(LocalConfig.objOItem_Session);
                if (objOItem_Result.GUID == LocalConfig.Globals.LState_Success.GUID)
                {
                    TimerActionType = TimerActionType.Stop;
                    var xmlReport = LocalConfig.objSession.GetXMLOfSession(LocalConfig.objOItem_Session);
                }
                else if (objOItem_Result.GUID == LocalConfig.Globals.LState_Error.GUID)
                {
                    TimerActionType = TimerActionType.Stop;
                    FormMessage = formMessage;
                    
                }
            }
            else
            {
                TimerActionType = TimerActionType.Stop;
                FormMessage = formMessage;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using System.IO;
using LocalizedTemplate_Module;
using CommandLineRun_Module.Converter;

namespace CommandLineRun_Module
{
    public partial class UserControl_ExecuteCode : UserControl
    {

        public event ScriptExecuted scriptExecuted;
        private ExecuteCode_WinformsViewModel viewModel;
        private delegate void viewModelChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e);

        

        public UserControl_ExecuteCode(clsLocalConfig LocalConfig, clsDataWork_CommandLineRun DataWork_CommandLineRun)
        {
            
            InitializeComponent();
            viewModel = new ExecuteCode_WinformsViewModel(LocalConfig, DataWork_CommandLineRun);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            viewModel.scriptExecuted += ViewModel_scriptExecuted;
            viewModel.InitViewModel();
            

        }

        private void ViewModel_scriptExecuted(string output, string error)
        {
            if (scriptExecuted != null)
            {
                scriptExecuted(output, error);
            }
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var delegPropertyChange = new viewModelChanged(ViewModel_PropertyChanged);
                this.Invoke(delegPropertyChange, sender, e);
            }
            else
            {
                if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_Encodings)
                {
                    comboBox_Encoding.DataSource = viewModel.Encodings;
                    comboBox_Encoding.ValueMember = viewModel.EncodingSelect_ValueMember;
                    comboBox_Encoding.DisplayMember = viewModel.EncodingSelect_DisplayMember;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_IsCodeReadonly)
                {
                    scintilla_Code.IsReadOnly = viewModel.IsCodeReadonly;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_IsCodeParsedReadonly)
                {
                    scintilla_CodeParsed.IsReadOnly = viewModel.IsCodeParsedReadonly;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_CodeText)
                {
                    scintilla_Code.Text = viewModel.CodeText;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_CodeParsedText)
                {
                    scintilla_CodeParsed.Text = viewModel.CodeParsedText;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_IsEnabledExec)
                {
                    button_Exec.Enabled = viewModel.IsEnabledExec;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_IsEnabledSave)
                {
                    button_Save.Enabled = viewModel.IsEnabledSave;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_IsCMDRLReadOnly)
                {
                    textBox_CMDRL.Enabled = viewModel.IsCMDRLReadOnly;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_ProgrammingLanguageName)
                {
                    textBox_ProgrammingLanguage.Text = viewModel.ProgrammingLanguageName;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_CMDRLText)
                {
                    textBox_CMDRL.Text = viewModel.CMDRLText;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_SyntaxHighlighter)
                {
                    scintilla_Code.ConfigurationManager.Language = viewModel.SyntaxHighlighter;
                    scintilla_CodeParsed.ConfigurationManager.Language = viewModel.SyntaxHighlighter;
                }
                else if (e.PropertyName == NotifyChanges.ExecuteCodeViewModel_FormMessage)
                {
                    MessageBox.Show(this, viewModel.FormMessage.Message, viewModel.FormMessage.Short, FormMessageToButtonConverter.Convert(viewModel.FormMessage), FormMessageToIconConverter.Convert(viewModel.FormMessage));
                }
            }

        }

        

        public void InitializeCodeView(clsOntologyItem OItem_Cmdlr, bool doExecute = false)
        {
            viewModel.InitializeCodeView(OItem_Cmdlr, doExecute);      
        }

        public void InitializeCodeView(clsOntologyItem OItem_Cmdlr,
                                       clsDataWork_TextPOrReportsToCommandLine objDataWork_ReportsToCommandLine,
                                       DataGridView dataGridView_Report,
                                       List<KeyValuePair<string, string>> columnFieldList)
        {
            viewModel.InitializeCodeView(OItem_Cmdlr, objDataWork_ReportsToCommandLine, dataGridView_Report, columnFieldList);
        }

        private void button_Exec_Click(object sender, EventArgs e)
        {
            viewModel.OpenExecutionDialog(this);
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            viewModel.ExportCode(this);
        }

        private void comboBox_Encoding_SelectedIndexChanged(object sender, EventArgs e)
        {
            viewModel.ScriptEncoding = ((EncodingInfo) comboBox_Encoding.SelectedItem).GetEncoding();
        }

        private void scintilla_CodeParsed_KeyDown(object sender, KeyEventArgs e)
        {
            if (Control.ModifierKeys.HasFlag(Keys.Control))
            {
                if (e.KeyCode == Keys.Space)
                {
                    viewModel.OpenAutoCorrection(this, scintilla_CodeParsed.Caret.Position, scintilla_CodeParsed.GetWordFromPosition(scintilla_CodeParsed.Caret.Position));

                }
            }
        }

    }
}

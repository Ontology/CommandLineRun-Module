﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Ontology_Module;
using OntologyClasses.BaseClasses;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace CommandLineRun_Module
{
    public class clsDataWork_CodeSniplets
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_CodeSnipplets;
        private OntologyModDBConnector objDBLevel_SyntaxHighlight;
        private OntologyModDBConnector objDBLevel_ProgrammingLanguage;

        public clsOntologyItem OItem_CodeSnipplet { get; private set; }
        public clsObjectAtt OAItem_Code { get; private set; }
        public clsOntologyItem OItem_SyntaxHighlight { get; private set; }
        public clsOntologyItem OItem_ProgrammingLanguage { get; private set; }

        public clsOntologyItem GetData_CodeSnipplet(clsOntologyItem OItem_CodeSnipplet)
        {
            this.OItem_CodeSnipplet = OItem_CodeSnipplet;
            OAItem_Code = null;
            var result = objLocalConfig.Globals.LState_Success.Clone();

            var searchCode = new List<clsObjectAtt> { new clsObjectAtt { ID_Object = OItem_CodeSnipplet.GUID,
                        ID_AttributeType = objLocalConfig.OItem_attributetype_code.GUID } };

            result = objDBLevel_CodeSnipplets.GetDataObjectAtt(searchCode, doIds:false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                OAItem_Code = objDBLevel_CodeSnipplets.ObjAtts.FirstOrDefault();
            }

            return result;
        }

        public clsOntologyItem GetData_ProgramingLanguage()
        {
            var result = objLocalConfig.Globals.LState_Success.Clone();

            var searchPl = new List<clsObjectRel> { new clsObjectRel {ID_Object = OItem_CodeSnipplet.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_is_written_in.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_programing_language.GUID}};

            result = objDBLevel_ProgrammingLanguage.GetDataObjectRel(searchPl, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                OItem_ProgrammingLanguage = objDBLevel_ProgrammingLanguage.ObjectRels.Select(pl => new clsOntologyItem
                {
                    GUID = pl.ID_Other,
                    Name = pl.Name_Other,
                    GUID_Parent = pl.ID_Parent_Other,
                    Type = pl.Ontology
                }).FirstOrDefault();
            }

            return result;
        }

        public clsOntologyItem GetData_SyntaxHighlight(clsOntologyItem OItem_ProgrammingLanguage)
        {
            OItem_SyntaxHighlight = null;
            var result = objLocalConfig.Globals.LState_Success.Clone();

            var searchHighlight = new List<clsObjectRel> { new clsObjectRel { ID_Other = OItem_ProgrammingLanguage.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Object = objLocalConfig.OItem_class_syntax_highlighting__scintillanet_.GUID } };

            result = objDBLevel_SyntaxHighlight.GetDataObjectRel(searchHighlight, doIds: false);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                OItem_SyntaxHighlight = objDBLevel_SyntaxHighlight.ObjectRels.Select(syn => new clsOntologyItem
                {
                    GUID = syn.ID_Object,
                    Name = syn.Name_Object,
                    GUID_Parent = syn.ID_Parent_Object,
                    Type = objLocalConfig.Globals.Type_Object
                }).ToList().FirstOrDefault();
            }

            return result;
        }

        public clsDataWork_CodeSniplets(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        public clsDataWork_CodeSniplets(Globals Globals)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_CodeSnipplets = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_SyntaxHighlight = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_ProgrammingLanguage = new OntologyModDBConnector(objLocalConfig.Globals);
        }
    }
}

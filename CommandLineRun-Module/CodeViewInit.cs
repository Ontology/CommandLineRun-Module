﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    [Flags]
    public enum CodeViewType
    {
        Unknown = 0,
        WithReport = 1,
        Root = 2,
        OItem = 4
    }
    public class CodeViewInit
    {

        public CodeViewType CodeViewType { get; set; }
        public clsOntologyItem OItemInit { get; set; }

        public CodeViewInit(string idSelectedNode, string textSelectedNode, bool isReportRegistered, clsLocalConfig localConfig)
        {
            if (idSelectedNode == localConfig.Globals.Root.GUID)
            {
                if (!isReportRegistered)
                {
                    OItemInit = localConfig.Globals.Root;
                    CodeViewType = CodeViewType.Root;


                }
                else
                {
                    CodeViewType = CodeViewType.Root | CodeViewType.WithReport;
                    OItemInit = localConfig.Globals.Root;
                    
                    
                }

            }
            else
            {
                OItemInit = new clsOntologyItem
                {
                    GUID = idSelectedNode,
                    Name = textSelectedNode,
                    GUID_Parent = localConfig.OItem_class_comand_line__run_.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                if (!isReportRegistered)
                {
                    CodeViewType = CodeViewType.OItem;
                }
                else
                {
                    CodeViewType = CodeViewType.OItem | CodeViewType.WithReport;
                    
                }

            }
        }
    }
}
 
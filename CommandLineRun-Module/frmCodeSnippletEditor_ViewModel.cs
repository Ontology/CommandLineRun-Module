﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public class frmCodeSnippletEditor_ViewModel
    {
        private clsLocalConfig objLocalConfig;
        public frmCodeSnippletEditor_ViewModel(Globals globals)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }
        }
    }
}

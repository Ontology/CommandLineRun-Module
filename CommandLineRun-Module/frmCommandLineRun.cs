﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using System.Linq;
using CommandLineRun_Module.Converter;

namespace CommandLineRun_Module
{
    

    public partial class frmCommandLineRun : Form
    {
        private frmCommandLineRun_WinFormsViewModel viewModel;
        private delegate void viewModelChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e);
       
        private UserControl_CommandLineRunTree objUserControl_CommandLineTree;
        private UserControl_ExecuteCode objUserControl_ExecuteCode;

        
        public event AppliedCommandLineRun appliedItem;
        

        public void CreateScriptOfTextParserOrReport(List<KeyValuePair<string, string>> columnFieldList, DataGridView dataSource)
        {
            viewModel.CreateScriptOfTextParserOrReport(columnFieldList, dataSource);
        }

        public frmCommandLineRun()
        {
            InitializeComponent();
            Initialize();
            viewModel.InitForm();
        }

        public frmCommandLineRun(Globals globals, clsOntologyItem commandLineRun)
        {
            InitializeComponent();
            Initialize(globals);
            viewModel.InitForm();
            objUserControl_CommandLineTree.SelectNode(commandLineRun);
        }

        private void Initialize(Globals globals = null)
        {
            
            if (globals != null)
            {
                viewModel = new frmCommandLineRun_WinFormsViewModel(globals);
            }
            else
            {
                viewModel = new frmCommandLineRun_WinFormsViewModel();
            }

            
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            viewModel.closeForm += ViewModel_closeForm;
            viewModel.appliedItem += ViewModel_appliedItem;

        }

        private void ViewModel_appliedItem()
        {
            if (this.InvokeRequired)
            {
                var applied = new AppliedCommandLineRun(ViewModel_appliedItem);
                this.Invoke(applied);
            }
            else
            {
                if (appliedItem != null)
                {
                    appliedItem();
                }
            }

        }

        private void ViewModel_closeForm(CloseType closeType)
        {
            if (this.InvokeRequired)
            {
                var closeDeleg = new CloseForm(ViewModel_closeForm);
                this.Invoke(closeDeleg, closeType);
            } 
            else
            {
                viewModel.PropertyChanged -= ViewModel_PropertyChanged;
                DialogResult = FormCloseTypeToDialogResultConverter.Convert(closeType);
                Close();
            }
                
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var delegPropertyChange = new viewModelChanged(ViewModel_PropertyChanged);
                this.Invoke(delegPropertyChange, sender, e);
            }
            else
            {
                if (e.PropertyName == NotifyChanges.FrmCommandLineRun_ViewModelType)
                {
                    if (viewModel.ViewModelType == ViewModelType.CommandLineAndShellOutput)
                    {
                        objUserControl_CommandLineTree = new UserControl_CommandLineRunTree(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun);
                        objUserControl_CommandLineTree.selectedNode += objUserControl_CommandLineTree_selectedNode;
                        objUserControl_CommandLineTree.Dock = DockStyle.Fill;
                        splitContainer1.Panel1.Controls.Add(objUserControl_CommandLineTree);


                        objUserControl_ExecuteCode = new UserControl_ExecuteCode(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun);
                        objUserControl_ExecuteCode.scriptExecuted += objUserControl_ExecuteCode_scriptExecuted;
                        objUserControl_ExecuteCode.Dock = DockStyle.Fill;
                        splitContainer1.Panel2.Controls.Add(objUserControl_ExecuteCode);

                        
                    }
                    else if (viewModel.ViewModelType == ViewModelType.Report)
                    {
                        objUserControl_CommandLineTree = new UserControl_CommandLineRunTree(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun);
                        objUserControl_CommandLineTree.selectedNode += objUserControl_CommandLineTree_selectedNode;
                        objUserControl_CommandLineTree.appliedCommandLineRun += objUserControl_CommandLineTree_appliedCommandLineRun;
                        objUserControl_CommandLineTree.Dock = DockStyle.Fill;
                        splitContainer1.Panel1.Controls.Add(objUserControl_CommandLineTree);

                        objUserControl_ExecuteCode = new UserControl_ExecuteCode(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun);
                        objUserControl_ExecuteCode.scriptExecuted += objUserControl_ExecuteCode_scriptExecuted;
                        objUserControl_ExecuteCode.Dock = DockStyle.Fill;
                        splitContainer1.Panel2.Controls.Add(objUserControl_ExecuteCode);
                    }
                    else if (viewModel.ViewModelType == ViewModelType.Standard)
                    {
                        objUserControl_CommandLineTree = new UserControl_CommandLineRunTree(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun);
                        objUserControl_CommandLineTree.selectedNode += objUserControl_CommandLineTree_selectedNode;
                        objUserControl_CommandLineTree.Dock = DockStyle.Fill;
                        splitContainer1.Panel1.Controls.Add(objUserControl_CommandLineTree);


                        objUserControl_ExecuteCode = new UserControl_ExecuteCode(viewModel.LocalConfig, viewModel.DataWork_CommandLineRun);
                        objUserControl_ExecuteCode.scriptExecuted += objUserControl_ExecuteCode_scriptExecuted;
                        objUserControl_ExecuteCode.Dock = DockStyle.Fill;
                        splitContainer1.Panel2.Controls.Add(objUserControl_ExecuteCode);
                    }
                    
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRun_CommandLineRunDetermined)
                {
                    if (viewModel.CommandLineRunDetermined)
                    {
                        objUserControl_CommandLineTree.InitializeTree();
                        objUserControl_CommandLineTree.Applyable = true;
                        objUserControl_CommandLineTree.MarkNodes(viewModel.DataWork_ReportsToCommandLine.CommandLineRunList);
                    }
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRun_Applyable)
                {
                    objUserControl_CommandLineTree.Applyable = viewModel.Applyable;
                    if (viewModel.Applyable)
                    {
                        objUserControl_CommandLineTree.appliedCommandLineRun += objUserControl_CommandLineTree_appliedCommandLineRun;
                    }
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRun_AutoRun)
                {
                    if (viewModel.AutoRun)
                    {
                        objUserControl_ExecuteCode.InitializeCodeView(viewModel.DataWork_CommandLineRun.OItem_Object, true);
                        objUserControl_CommandLineTree.InitializeTree();
                    }
                    else
                    {
                        objUserControl_CommandLineTree.InitializeTree();
                        
                    }
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRun_FormHeaderText)
                {
                    this.Text = viewModel.FormHeaderText;
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRun_ShowSnippletEditor)
                {
                    var objFrm_ScriptingEditor = new frmScriptingEditor(viewModel.LocalConfig, viewModel.CodeSnipplet);
                    objFrm_ScriptingEditor.ShowDialog(this);
                    Environment.Exit(0);
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRunWinForms_RegisteredReportName)
                {
                    this.toolStripTextBox_RegisteredReport.Text = viewModel.RegisteredReportName;
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRunWinForms_TimerActionType)
                {
                    if (viewModel.TimerActionType == TimerActionType.Stop)
                    {
                        Timer_Session.Stop();
                    }
                    else
                    {
                        Timer_Session.Start();
                    }
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRunWinForms_FormMessage)
                {
                    MessageBox.Show(this,
                        viewModel.FormMessage.Message,
                        viewModel.FormMessage.Short,
                        FormMessageToButtonConverter.Convert(viewModel.FormMessage),
                        FormMessageToIconConverter.Convert(viewModel.FormMessage));
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRunWinForms_CodeViewInit)
                {
                    
                    if (viewModel.CodeViewInit.CodeViewType.HasFlag(CodeViewType.WithReport))
                    {
                        objUserControl_ExecuteCode.InitializeCodeView(viewModel.CodeViewInit.OItemInit, viewModel.DataWork_ReportsToCommandLine, viewModel.DataSource, viewModel.ColumnFieldList);
                    }
                    else
                    {
                        objUserControl_ExecuteCode.InitializeCodeView(viewModel.CodeViewInit.OItemInit);
                    }
                    
                }
                else if (e.PropertyName == NotifyChanges.FrmCommandLineRun_ViewModelError)
                {
                    if (viewModel.ViewModelError.HasFlag(ViewModelError.ReportsToCommandLineInstance))
                    {
                        throw new Exception("Reports to CommandLine Instance kann nicht ermittelt werden!");
                    }
                }
            }
        }

        

        public frmCommandLineRun(string[] commandLine, clsShellOutput shellOutput)
        {
            InitializeComponent();
            Initialize();
            viewModel.InitFormCommandLineAndShellOutput(commandLine, shellOutput);

        }
        

        public frmCommandLineRun(Globals Globals, clsOntologyItem OItem_TextParserOrReport, bool forReports = true)
        {
            InitializeComponent();
            Initialize(Globals);
            viewModel.InitFormReport(OItem_TextParserOrReport, forReports);
            
            
            

            
            
        }

        
        

        void objUserControl_CommandLineTree_appliedCommandLineRun()
        {
            viewModel.CommandLineRunApplied();

        }

        

        void objUserControl_ExecuteCode_scriptExecuted(string output, string error)
        {
            
            viewModel.PropertyChanged -= ViewModel_PropertyChanged;
            viewModel.closeForm -= ViewModel_closeForm;
            viewModel.appliedItem -= ViewModel_appliedItem;

            viewModel.ScriptExecuted(output, error);

            this.Close();
        }

        


        void objUserControl_CommandLineTree_selectedNode(TreeNode selectedNode)
        {
            viewModel.SelectedCommandLineRun(selectedNode);
        }

      
        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Timer_Session_Tick(object sender, EventArgs e)
        {
            viewModel.SessionTick();
        }
    }
}

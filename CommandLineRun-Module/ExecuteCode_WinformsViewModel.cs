﻿using LocalizedTemplate_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandLineRun_Module
{
    public class ExecuteCode_WinformsViewModel : ExecuteCode_ViewModel
    {
        private frmScriptExecution objFrmScriptExecution;
        private frmAutoCorrection objFrmAutoCorrection;
        public event ScriptExecuted scriptExecuted;

        private SaveFileDialog saveFileDialog_Script = new SaveFileDialog();

        public ExecuteCode_WinformsViewModel(clsLocalConfig localConfig, clsDataWork_CommandLineRun dataWork_CommandLineRun) : base(localConfig, dataWork_CommandLineRun)
        {
         
        }

        public ExecuteCode_WinformsViewModel(Globals globals) : base(globals)
        {
        }

        public void InitializeCodeView(clsOntologyItem OItem_Cmdlr,
                                       clsDataWork_TextPOrReportsToCommandLine objDataWork_ReportsToCommandLine,
                                       DataGridView dataGridView_Report,
                                       List<KeyValuePair<string, string>> columnFieldList)
        {


            Clear();
            this.OItem_CMDLR = OItem_Cmdlr;

            if (OItem_CMDLR != null && OItem_CMDLR.GUID != LocalConfig.Globals.Root.GUID)
            {
                CMDRLText = OItem_CMDLR.Name;
                IsCMDRLReadOnly = false;

                var codes =
                    DataWork_CommandLineRun.Codes.Where(code => code.ID_CommandLineRun == OItem_CMDLR.GUID)
                                              .ToList();

                var subCmdrls = DataWork_CommandLineRun.GetSubCmdlrs(OItem_Cmdlr);

                codes.AddRange(from objCode in DataWork_CommandLineRun.Codes
                               join subCmdrl in subCmdrls on objCode.ID_CommandLineRun equals subCmdrl.ID_Other
                               join codeExist in codes on objCode.ID_CommandLineRun equals codeExist.ID_CommandLineRun into codesExist
                               from codeExist in codesExist.DefaultIfEmpty()
                               where codeExist == null
                               orderby subCmdrl.OrderID
                               select objCode);

                IsCodeReadonly = false;
                IsCodeParsedReadonly = false;
                
                var variablesToReportFields = objDataWork_ReportsToCommandLine.GetVariableToField();
                var dataExtractConfig = (from variableToField in variablesToReportFields
                                         join columnField in columnFieldList on variableToField.ID_Field
                                             equals columnField.Value
                                         select new { variableToField, columnField }).ToList();
                ScriptEncoding = null;
                codes.ForEach(code =>
                {
                    EncodingInfo encTemp = null;
                    if (code.Name_Encoding != null)
                    {
                        encTemp = Encodings.Where(enc => enc.DisplayName.ToLower() == code.Name_Encoding.ToLower()).FirstOrDefault();
                        if (encTemp != null)
                        {
                            if (ScriptEncoding == null)
                            {
                                ScriptEncoding = encTemp.GetEncoding();
                            }
                            else
                            {
                                if (ScriptEncoding != encTemp.GetEncoding())
                                {
                                    ScriptEncoding = null;
                                }
                            }
                        }
                    }


                    if (dataExtractConfig.Any(dc => dc.variableToField.ID_CommandLineRun == code.ID_CommandLineRun))
                    {
                        if (dataGridView_Report.SelectedRows.Count == 0)
                        {
                            var fieldCode = "";
                            dataGridView_Report.Rows.Cast<DataGridViewRow>().ToList().ForEach(dgvr =>
                            {
                                var text = code.CodeParsed;
                                dataExtractConfig.ForEach(dat =>
                                {
                                    var value =
                                        System.Web.HttpUtility.HtmlDecode(
                                            dgvr.Cells[dat.columnField.Key].Value.ToString());

                                    dat.variableToField.RemoveCharacters.ForEach(removeChar =>
                                    {
                                        value = removeChar.RemoveCharacters(value);
                                    });

                                    dat.variableToField.FieldReplaces.ForEach(fieldReplace =>
                                    {
                                        value = fieldReplace.Replace(value);
                                    });

                                    text = text.Replace("@" + dat.variableToField.Name_Variable + "@",
                                        value);

                                    
                                     
                                });
                                fieldCode += text;

                                
                                
                            });

                            objDataWork_ReportsToCommandLine.RemoveCharactersCmdLineRun.ForEach(removeChar =>
                            {
                                fieldCode = removeChar.RemoveCharacters(fieldCode);
                            });

                            
                            CodeParsedText += fieldCode;
                        }
                        else
                        {
                            var fieldCode = "";
                            dataGridView_Report.SelectedRows.Cast<DataGridViewRow>().ToList().ForEach(dgvr =>
                            {
                                var text = code.CodeParsed;


                                dataExtractConfig.ForEach(dat =>
                                {
                                    var value =
                                        System.Web.HttpUtility.HtmlDecode(
                                            dgvr.Cells[dat.columnField.Key].Value.ToString());
                                    text = text.Replace("@" + dat.variableToField.Name_Variable + "@",
                                        value);

                                    
                                });

                                fieldCode += text;

                            });

                            objDataWork_ReportsToCommandLine.RemoveCharactersCmdLineRun.ForEach(removeChar =>
                            {
                                fieldCode = removeChar.RemoveCharacters(fieldCode);
                            });

                            
                            CodeParsedText += fieldCode;
                        }


                        

                        CodeText += code.Code;

                    }
                    else
                    {


                        CodeText += code.Code;
                        CodeParsedText += code.CodeParsed;
                    }

                });


                






                var programmingLanguages = (from code in codes
                                            group code by new { code.ID_ProgrammingLanguage, code.Name_ProgrammingLanguage }
                                                into pls
                                            where pls.Key.ID_ProgrammingLanguage != null
                                            select pls.Key).ToList();
                OItem_ProgrammingLanguage = programmingLanguages.Count == 1 ? new clsOntologyItem
                {
                    GUID = programmingLanguages.First().ID_ProgrammingLanguage,
                    Name = programmingLanguages.First().Name_ProgrammingLanguage,
                    GUID_Parent = LocalConfig.OItem_class_programing_language.GUID,
                    Type = LocalConfig.Globals.Type_Object
                } : null;
                ProgrammingLanguageName = programmingLanguages.Count == 1 ? programmingLanguages.First().Name_ProgrammingLanguage : "";

                if (programmingLanguages.Count == 1)
                {
                    var exeConfig =
                        DataWork_CommandLineRun.ExecutableConfigurations.Where(
                            exec => exec.ID_ProgrammingLanguage == programmingLanguages.First().ID_ProgrammingLanguage)
                                                  .ToList();

                    var syntaxHighlightPL = (from code in codes
                                             group code by code.Name_SyntaxHighlighting
                                                 into highlights
                                             where highlights.Key != null
                                             select highlights.Key).ToList();

                    ExecutionConfiguration = exeConfig.FirstOrDefault();

                    if (syntaxHighlightPL.Count == 1 && ExecutionConfiguration != null)
                    {
                        ExecutionConfiguration.Name_SyntaxHighlight = syntaxHighlightPL.First();
                        IsEnabledExec = true;
                    }

                }


                var syntaxHighlight = (from code in codes
                                       group code by code.Name_SyntaxHighlighting
                                           into highlights
                                       where highlights.Key != null
                                       select highlights.Key).ToList();

                SyntaxHighlighter = syntaxHighlight.Count == 1 ? syntaxHighlight.First() : "";
                IsCodeReadonly = true;
                IsCodeParsedReadonly = true;

                IsEnabledSave = true;

                if (ScriptEncoding != null)
                {

                    EncodingItem = Encodings.FirstOrDefault(enc => enc.CodePage == ScriptEncoding.CodePage);

                }

            }
        }

        public void OpenExecutionDialog(IWin32Window parentWindow)
        {
            objFrmScriptExecution = new frmScriptExecution(ExecutionConfiguration, CodeParsedText);
            objFrmScriptExecution.ShowDialog(parentWindow);


        }

        public void OpenAutoCorrection(IWin32Window parentWindow, int position, string wordOfPosition)
        {
            if (OItem_ProgrammingLanguage != null)
            {
                if (objFrmAutoCorrection == null)
                {
                    objFrmAutoCorrection = new frmAutoCorrection(LocalConfig.Globals);
                    AutoCorrectorUsable = objFrmAutoCorrection.SetAutoCorrectorByRef(OItem_ProgrammingLanguage);

                    if (AutoCorrectorUsable.GUID == LocalConfig.Globals.LState_Error.GUID)
                    {
                        FormMessage = new FormMessage("Der Autokorrektor kann nicht genutzt werden!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
                    }
                }

                PositionOfWord = position;
                WordOfPosition = wordOfPosition;
                objFrmAutoCorrection.ValueToSearch = WordOfPosition;
                objFrmAutoCorrection.Show();


            }
        }

        public void ExportCode(IWin32Window parentWindow)
        {
            if (saveFileDialog_Script.ShowDialog(parentWindow) == DialogResult.OK)
            {
                var strDestPath = saveFileDialog_Script.FileName;
                try
                {
                    TextWriter textStream;

                    if (ScriptEncoding != null)
                    {
                        textStream = new StreamWriter(strDestPath, false, ScriptEncoding);
                    }
                    else
                    {
                        textStream = new StreamWriter(strDestPath, false);
                    }

                    textStream.Write(CodeParsedText);
                    textStream.Close();
                }
                catch (Exception ex)
                {
                    FormMessage = new FormMessage("Das Script konnte nicht gespeichert werden!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
                    
                }

            }
        }

        public void InitializeCodeView(clsOntologyItem oItem_Cmdrl, IWin32Window parentForm, bool doExecute = false)
        {
            base.InitializeCodeView(oItem_Cmdrl, doExecute = false);
            if (oItem_Cmdrl != null && oItem_Cmdrl.GUID != LocalConfig.Globals.Root.GUID)
            {
                if (doExecute)
                {
                    if (ExecutionConfiguration != null)
                    {
                        var objShellOutput = new clsShellOutput();
                        objFrmScriptExecution = new frmScriptExecution(ExecutionConfiguration, CodeParsedText, true, objShellOutput);

                        scriptExecuted(objShellOutput.OutputText, objShellOutput.ErrorText);

                    }

                }

                if (ExecutionConfiguration != null && ExecutionConfiguration.Name_Extension != null)
                {
                    saveFileDialog_Script.Filter = ExecutionConfiguration.Name_ProgrammingLanguage + "|*" + (!ExecutionConfiguration.Name_Extension.StartsWith(".") ? "." : "") + ExecutionConfiguration.Name_Extension;
                }
                else
                {
                    saveFileDialog_Script.Filter = "Alle Dateien|*.*";
                }
            }
        }
    }
}

﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandLineRun_Module
{
    public class frmCommandLineRun_WinFormsViewModel : frmCommandLineRun_ViewModel
    {

        public DataGridView dataSource;
        public DataGridView DataSource
        {
            get { return dataSource; }
            set
            {
                dataSource = value;
                RaisePropertyChanged(NotifyChanges.FrmCommandLineRunWinForms_DataSource);
            }
        }

        public void CreateScriptOfTextParserOrReport(List<KeyValuePair<string, string>> columnFieldList, DataGridView dataSource)
        {
            ColumnFieldList = columnFieldList;
            DataSource = dataSource;
            RegisteredReportName = Report.Name;
            
            ReportRegistered = true;
        }

        public void SelectedCommandLineRun(TreeNode selectedNode)
        {
            CodeViewInit = new CodeViewInit(selectedNode.Name, selectedNode.Text, ReportRegistered, LocalConfig);
            
        }

        public frmCommandLineRun_WinFormsViewModel(Globals globals) : base(globals)
        {
          

        }

        public frmCommandLineRun_WinFormsViewModel() : base()
        {
            
        }
    }
}

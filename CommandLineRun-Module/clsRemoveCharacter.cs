﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public class clsRemoveCharacter
    {
        public string ID_RemoveCharacter { get; set; }
        public string Name_RemoveCharacter { get; set; }
        public bool AtBegin { get; set; }
        public long Count { get; set; }
        public long Offset { get; set; }

        public string RemoveCharacters(string value)
        {
            if (AtBegin)
            {
                return value.Remove((int)Offset, (int)Count);
            }
            else
            {
                return value.Remove(value.Length - ((int)Offset), (int)Count);
            }
        }
    }
}

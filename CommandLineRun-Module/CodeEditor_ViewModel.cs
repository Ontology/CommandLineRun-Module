﻿using CommandLineRun_Module.BaseClasses;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineRun_Module
{
    public delegate void SavedCodeSnipplet(clsOntologyItem OItem_CodeSnipplet);
    public class CodeEditor_ViewModel : ViewModelBase
    {
        
        public event SavedCodeSnipplet savedCodeSnipplet;

        public clsLocalConfig LocalConfig { get; private set; }
        public clsDataWork_CodeSniplets DataWork_CodeSnipplets;

        private clsTransaction objTransaction;
        private clsRelationConfig objRelationConfig;

        private List<clsValue> values;
        public List<clsValue> Values
        {
            get { return values; }
            set
            {
                values = value;
                if (values != null && values.Any())
                {
                    IsEnabled_ReplaceVar = true;
                }
                else
                {
                    IsEnabled_ReplaceVar = false;
                }
                ReplaceVariables();
            }
        }

        public CodeEditor_ViewModel(clsLocalConfig localConfig)
        {
            LocalConfig = localConfig;

        }

        private bool isEnabled_Save;
        public bool IsEnabled_Save
        {
            get { return isEnabled_Save; }
            set
            {
                isEnabled_Save = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_IsEnabled_Save);
            }
        }

        private bool isEnabled_ReplaceVar;
        public bool IsEnabled_ReplaceVar
        {
            get { return isEnabled_ReplaceVar; }
            set
            {
                isEnabled_ReplaceVar = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_IsEnabled_ReplaceVar);
            }
        }

        private bool isChangedText;
        public bool IsChangedText
        {
            get { return isChangedText; }
            set
            {
                isChangedText = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_IsChangedText);
            }
        }

        private bool isEditorLocked;
        public bool IsEditorLocked
        {
            get { return isEditorLocked; }
            set
            {
                isEditorLocked = value;
                toogleLockImage();
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_IsEditorLocked);
            }
        }

        private object imageItemLock;
        public object ImageItemLock
        {
            get { return imageItemLock; }
            set
            {
                imageItemLock = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_ImageItemLock);
            }
        }

        private void toogleLockImage()
        {
            if (!IsEditorLocked)
            {
                IsEnabledCodeEditor = true;
                IsEnabled_ReplaceVar = values != null ? values.Any() ? true : false : false;

                ImageItemLock = CommandLineRun_Module.Properties.Resources.padlock_aj_ashton_open_01;
            }
            else
            {
                IsEnabledCodeEditor = false;
                IsEnabled_ReplaceVar = false;
                ImageItemLock = CommandLineRun_Module.Properties.Resources.padlock_aj_ashton_01;
            }
        }

        private bool isEnabledCodeEditor;
        public bool IsEnabledCodeEditor
        {
            get { return isEnabledCodeEditor; }
            set
            {
                isEnabledCodeEditor = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_IsEnabledCodeEditor);
            }
        }

        private string codeText;
        public string CodeText
        {
            get { return codeText; }
            set
            {
                codeText = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_CodeText);
            }
        }

        private int lineCount;
        public int LineCount
        {
            get { return lineCount; }
            set
            {
                lineCount = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_LineCount);
            }
        }

        public string LineCountLabel
        {
            get { return lineCount.ToString(); }
        }

        private FormMessage formMessage;
        public FormMessage FormMessage
        {
            get { return formMessage; }
            set
            {
                formMessage = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_FormMessage);
            }
        }

        private clsOntologyItem oItem_CodeSnipplet;
        public clsOntologyItem OItem_CodeSnipplet
        {
            get { return oItem_CodeSnipplet; }
            set
            {
                oItem_CodeSnipplet = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_OItem_CodeSnipplet);
            }
        }

        private clsOntologyItem oItem_ProgrammingLanguage;
        public clsOntologyItem OItem_ProgrammingLanguage
        {
            get { return oItem_ProgrammingLanguage; }
            set
            {
                oItem_ProgrammingLanguage = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_OItem_ProgrammingLanguage);
            }
        }


        private string syntxHighlighter;
        public string SyntxHighlighter
        {
            get { return syntxHighlighter; }
            set
            {
                syntxHighlighter = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_SyntxHighlighter);
            }
        }

        private int positionOfWord;
        public int PositionOfWord
        {
            get { return positionOfWord; }
            set
            {
                positionOfWord = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_PositionOfWord);
            }
        }

        private string wordOfPosition;
        public string WordOfPosition
        {
            get { return wordOfPosition; }
            set
            {
                wordOfPosition = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_WordOfPosition);
            }
        }

        private int ixCompare;
        public int IxCompare
        {
            get { return ixCompare; }
            set
            {
                ixCompare = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_IxCompare);
            }
        }

        private CompareItem compareItem;
        public CompareItem CompareItem
        {
            get { return compareItem; }
            set
            {
                compareItem = value;
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_CompareItem);
            }
        }

        private clsOntologyItem autoCorrectorUsable;
        public clsOntologyItem AutoCorrectorUsable
        {
            get { return autoCorrectorUsable; }
            set
            {
                autoCorrectorUsable = value;
                if (autoCorrectorUsable.GUID == LocalConfig.Globals.LState_Error.GUID)
                {
                    FormMessage = new FormMessage("Der Autokorrektor kann nicht genutzt werden!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
                }
                RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_AutoCorrectorUsable);
            }
        }

        public void CorrectorItem(string wordToCompare, clsOntologyItem oItemSelected)
        {
            if (wordToCompare == WordOfPosition)
            {
                compareItem = new CompareItem
                {
                    TextToCompare = wordToCompare,
                    TextToExchange = oItemSelected.Name
                };

                var lengthOfWord = wordToCompare.Length;
                var start = positionOfWord >= lengthOfWord ? positionOfWord - lengthOfWord : 0;
                for (int i = start; i < positionOfWord; i++)
                {
                    compareItem.IxStart = i;
                    compareItem.End = i + lengthOfWord;
                    RaisePropertyChanged(NotifyChanges.CodeEditorViewModel_CompareItem);
                }
            }
        }


        public void GetCodeSnipplet()
        {
            IsChangedText = true;
            var codeText = "";
            
            
            if (OItem_CodeSnipplet != null)
            {
                var result = DataWork_CodeSnipplets.GetData_CodeSnipplet(OItem_CodeSnipplet);
                if (result.GUID == LocalConfig.Globals.LState_Success.GUID)
                {
                    IsEnabledCodeEditor = true;

                    codeText = DataWork_CodeSnipplets.OAItem_Code != null ? DataWork_CodeSnipplets.OAItem_Code.Val_String : "";

                    if (OItem_ProgrammingLanguage == null)
                    {
                        result = DataWork_CodeSnipplets.GetData_ProgramingLanguage();
                    }

                    if (result.GUID == LocalConfig.Globals.LState_Success.GUID)
                    {
                        OItem_ProgrammingLanguage = DataWork_CodeSnipplets.OItem_ProgrammingLanguage;
                    }

                    CodeText = codeText;
                }
                else
                {
                    IsEnabledCodeEditor = false;
                    
                }

            }

            

            IsChangedText = false;
        }

        public void ReplaceVariables()
        {
            var result = LocalConfig.Globals.LState_Success.Clone();
            var replaceCount = new List<int>();
            var startIx = 0;
            var nextIx = -1;
            foreach(var value in values)
            {
                nextIx = CodeText.IndexOf("@" + value.Name_Variable + "@", startIx);
                while ( nextIx != -1)
                {
                    replaceCount.Add(nextIx);
                    nextIx = CodeText.IndexOf("@" + value.Name_Variable + "@", nextIx);
                }
                if (replaceCount.Any())
                {
                    CodeText = CodeText.Replace("@" + value.Name_Variable + "@", value.Name_Value);
                }
                
                if (replaceCount.Any())
                {
                    result = SaveVariable(new clsOntologyItem { GUID = value.ID_Variable, Name = value.Name_Variable, GUID_Parent = LocalConfig.OItem_class_variable.GUID });
                    if (result.GUID== LocalConfig.Globals.LState_Error.GUID)
                    {
                        FormMessage = new FormMessage("Beim Löschen einer Variable ist ein Fehler aufgetreten!", "Fehler!", FormMessageType.Exclamation | FormMessageType.OK);
                        break;
                    }
                }
                else
                {
                    result = DeleteVariable(new clsOntologyItem { GUID = value.ID_Variable, Name = value.Name_Variable, GUID_Parent = LocalConfig.OItem_class_variable.GUID });
                    if (result.GUID == LocalConfig.Globals.LState_Error.GUID)
                    {
                        FormMessage = new FormMessage("Beim Löschen einer Variable ist ein Fehler aufgetreten!", "Fehler!", FormMessageType.Exclamation | FormMessageType.OK);
                        break;
                    }
                }
            }

            if (result.GUID == LocalConfig.Globals.LState_Error.GUID)
            {
                IsEnabledCodeEditor = false;
            }
        }

        

        private clsOntologyItem SaveVariable(clsOntologyItem OItem_Variable)
        {
            var result = LocalConfig.Globals.LState_Success.Clone();

            var saveVar = objRelationConfig.Rel_ObjectRelation(OItem_CodeSnipplet, OItem_Variable, LocalConfig.OItem_relationtype_contains);

            result = objTransaction.do_Transaction(saveVar);

            return result;
        }

        private clsOntologyItem DeleteVariable(clsOntologyItem OItem_Variable)
        {
            var result = LocalConfig.Globals.LState_Success.Clone();

            var delVar = objRelationConfig.Rel_ObjectRelation(OItem_CodeSnipplet, OItem_Variable, LocalConfig.OItem_relationtype_contains);

            result = objTransaction.do_Transaction(delVar, false, true);

            return result;
        }

        public void GetSyntaxHighlight()
        {
            if (OItem_ProgrammingLanguage != null)
            {
                var result = DataWork_CodeSnipplets.GetData_SyntaxHighlight(OItem_ProgrammingLanguage);
                if (result.GUID == LocalConfig.Globals.LState_Success.GUID)
                {
                    SyntxHighlighter = DataWork_CodeSnipplets.OItem_SyntaxHighlight != null ? DataWork_CodeSnipplets.OItem_SyntaxHighlight.Name : "";
                    
                }
                else
                {
                    SyntxHighlighter = "";
                }
            }
        }

        public void TextChanged(int lineCount)
        {
            LineCount = lineCount;
            if (!isChangedText)
            {
                IsEnabled_Save = true;
            }
        }

        public void SaveCode(string code)
        {
            //if (savedCodeSnipplet == null)
            //{
            //    savedCodeSnipplet += UserControl_CodeEditor_savedCodeSnipplet;
            //}
            CodeText = code;

            if (IsEnabledCodeEditor && !isChangedText)
            {
                objTransaction.ClearItems();

                var result = LocalConfig.Globals.LState_Success.Clone();

                if (OItem_CodeSnipplet == null)
                {
                    var guid = LocalConfig.Globals.NewGUID;
                    OItem_CodeSnipplet = new clsOntologyItem
                    {
                        GUID = guid,
                        Name = guid,
                        GUID_Parent = LocalConfig.OItem_class_code_snipplets.GUID,
                        Type = LocalConfig.Globals.Type_Object,
                        New_Item = true
                    };

                    result = objTransaction.do_Transaction(OItem_CodeSnipplet);

                    if (result.GUID == LocalConfig.Globals.LState_Success.GUID & OItem_ProgrammingLanguage != null)
                    {
                        var savePL = objRelationConfig.Rel_ObjectRelation(OItem_CodeSnipplet, OItem_ProgrammingLanguage, LocalConfig.OItem_relationtype_is_written_in);
                        result = objTransaction.do_Transaction(savePL, true);
                    }
                }
                else
                {
                    OItem_CodeSnipplet.New_Item = false;
                }


                if (result.GUID == LocalConfig.Globals.LState_Success.GUID)
                {
                    var saveCode = objRelationConfig.Rel_ObjectAttribute(OItem_CodeSnipplet, LocalConfig.OItem_attributetype_code, CodeText);

                    result = objTransaction.do_Transaction(saveCode, true);

                    if (result.GUID == LocalConfig.Globals.LState_Success.GUID)
                    {
                        IsEnabled_Save = false;
                        if (savedCodeSnipplet != null)
                        {
                            savedCodeSnipplet(OItem_CodeSnipplet);
                        }
                    }
                    else
                    {
                        FormMessage = new FormMessage("Beim Speichern des Codes ist ein Fehler aufgetreten!", "Fehler!", FormMessageType.OK | FormMessageType.Exclamation);
                        IsEnabledCodeEditor = false;
                    }
                }
                else
                {
                    objTransaction.rollback();
                }
            }

        }

        public CodeEditor_ViewModel(Globals globals)
        {
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }
        }

        public void Clear()
        {
            IsChangedText = true;
            IsEnabledCodeEditor = true;
            CodeText = "";
            IsEditorLocked = true;
            IsChangedText = false;
        }

        public void InitializeViewModel()
        {
            DataWork_CodeSnipplets = new clsDataWork_CodeSniplets(LocalConfig);
            objTransaction = new clsTransaction(LocalConfig.Globals);
            objRelationConfig = new clsRelationConfig(LocalConfig.Globals);

            IsEnabled_Save = false;
            IsEnabled_ReplaceVar = false;
            
        }
    }
}

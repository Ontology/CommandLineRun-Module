﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommandLineRun_Module
{
    
    public class CommandLineRunTree_WinformsViewModel : CommandLineRunTree_ViewModel
    {

        public event ClearCommandLineTreeNode clearCommandLineTreeNode;
        public int RootNodeCount { get; private set; }

        private clsOntologyItem result_TreeList;
        public clsOntologyItem Result_TreeList
        {
            get { return result_TreeList; }
            set
            {
                result_TreeList = value;
                RaisePropertyChanged(NotifyChanges.CommandLineRunTree_Result_TreeList);
            }
        }

        

        public CommandLineRunTree_WinformsViewModel(clsLocalConfig localConfig, clsDataWork_CommandLineRun dataWork_CommandLineRun):base(localConfig, dataWork_CommandLineRun)
        {

        }

        

        public void ResetMarkTimer()
        {
            MarkTimerActionType = MarkTimerActionType.Stop;
            MarkTimerActionType = MarkTimerActionType.Start;
        }

        public void FillTree()
        {
            if (clearCommandLineTreeNode != null)
            {
                clearCommandLineTreeNode();
            }

            var rootTreeItem = new CommandLineRunTreeItem { IdCommandLineRun = LocalConfig.Globals.Root.GUID, NameCommandLineRun = LocalConfig.Globals.Root.Name };
            rootTreeItem.TreeNodeItem = new TreeNode { Name = rootTreeItem.IdCommandLineRun, Text = rootTreeItem.NameCommandLineRun };
            var rootNode = (TreeNode)rootTreeItem.TreeNodeItem;
            NodeBackColor = rootNode.BackColor;
            NodeForeColor = rootNode.ForeColor;

            FillSubNodes(rootNode);

            if (result_TreeList.GUID == LocalConfig.Globals.LState_Success.GUID)
            {
                if (DataWork_CommandLineRun.OItem_Object != null && DataWork_CommandLineRun.OItem_Object.GUID_Parent == LocalConfig.OItem_class_comand_line__run_.GUID)
                {
                    var nodeItem = TreeItems.FirstOrDefault(treeItem => treeItem.IdCommandLineRun == DataWork_CommandLineRun.OItem_Object.GUID);

                    if (nodeItem != null)
                    {
                        SelectedTreeItem = nodeItem;
                    }
                }
            }

            rootNode.Expand();
            TreeItemRoot = rootTreeItem;
            TreeNodeCount = RootNodeCount.ToString();
        }

        private void FillSubNodes(TreeNode treeNodeParent)
        {
            var objOItem_Result = LocalConfig.Globals.LState_Success.Clone();

            if (treeNodeParent.Name == LocalConfig.Globals.Root.GUID)
            {
                List<clsOntologyItem> cmdrls;
                if (DataWork_CommandLineRun.OItem_Result_Filter.GUID == LocalConfig.Globals.LState_Success.GUID)
                {
                    cmdrls = (from cmdrl in DataWork_CommandLineRun.CommandLineRunObjects
                              join objFilter in DataWork_CommandLineRun.FilterList on cmdrl.GUID equals objFilter.GUID
                              join cmdrlTree in DataWork_CommandLineRun.CommandLineRunTreeRel on cmdrl.GUID equals
                                  cmdrlTree.ID_Other into cmdrlsTree
                              from cmdrlTree in cmdrlsTree.DefaultIfEmpty()
                              where cmdrlTree == null
                              select cmdrl).OrderBy(cmdrl => cmdrl.Name).ToList();
                }
                else
                {
                    cmdrls = (from cmdrl in DataWork_CommandLineRun.CommandLineRunObjects
                              join cmdrlTree in DataWork_CommandLineRun.CommandLineRunTreeRel on cmdrl.GUID equals
                                  cmdrlTree.ID_Other into cmdrlsTree
                              from cmdrlTree in cmdrlsTree.DefaultIfEmpty()
                              where cmdrlTree == null
                              select cmdrl).OrderBy(cmdrl => cmdrl.Name).ToList();
                }

                RootNodeCount = 0;
                RootNodeCount += cmdrls.Count;
                cmdrls.ForEach(cmdrl =>
                {
                    var treeNode = treeNodeParent.Nodes.Add(cmdrl.GUID, cmdrl.Name);
                    TreeItems.Add(new CommandLineRunTreeItem { IdCommandLineRun = cmdrl.GUID, NameCommandLineRun = cmdrl.Name, TreeNodeItem = treeNode });
                    FillSubNodes(treeNode);
                });
            }
            else
            {
                var cmdrls =
                    DataWork_CommandLineRun.CommandLineRunTreeRel.Where(tree => tree.ID_Object == treeNodeParent.Name)
                                                 .OrderBy(cmdrl => cmdrl.OrderID).ThenBy(cmdrl => cmdrl.Name_Other).ToList();

                RootNodeCount += cmdrls.Count;
                cmdrls.ForEach(cmdrl =>
                {
                    var treeNode = treeNodeParent.Nodes.Add(cmdrl.ID_Other, cmdrl.Name_Other);
                    TreeItems.Add(new CommandLineRunTreeItem { IdCommandLineRun = cmdrl.ID_Other, NameCommandLineRun = cmdrl.Name_Other, TreeNodeItem = treeNode });
                    FillSubNodes(treeNode);
                });
            }

            Result_TreeList = objOItem_Result;
        }

        


    }
}
